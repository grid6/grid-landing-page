<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Dewa United - Loket Tiket</title>

  <link rel="icon" type="image/ico" href="<?= base_url() ?>assets/img/favicon.ico"/>

  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
</head>
<body>

  <main>
    <section class="success-section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="w-75 m-auto text-center">
              <img src="<?= base_url() ?>assets/img/du-logo-universal.png" alt="dewa united" width="200">
              <h3 class="my-5">Admin Loket Tiket</h3>
            </div>
          </div>
          <div class="col-12">
            <div class="w-75 m-auto">
              <form method="POST" action="<?= base_url("auth/du/staff/token") ?>">
                <div class="mb-3">
                  <input type="text" class="form-control form-control-lg" id="inputEmail" name="userdu" placeholder="Username" required />
                </div>
                <div class="mb-3 position-relative">
                  <button type="button" class="position-absolute end-0 bg-transparent d-flex align-items-center h-100 toggle-password" toggle="#inputPassword">
                    <img src="<?= base_url() ?>assets/img/icon-eye.svg" alt="" />
                  </button>
                  <input type="password" class="form-control form-control-lg" id="inputPassword" name="passdu" placeholder="Password" required />
                </div>
                <button type="submit" class="btn btn-primary btn-lg w-100 mb-2">Login</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <footer class="fixed-bottom text-center">
    <section>
      <div class="container">
        <div class="d-flex align-items-center justify-content-center">
          <small class="text-secondary">&copy; Copyright Dewa United.</small>
        </div>
      </div>
    </section>
  </footer>

  <script src="<?= base_url() ?>assets/js/jquery-3.5.1.min.js"></script>
  <script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
  <script type="module" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule="" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js"></script>
  <script defer src="<?= base_url() ?>assets/js/slick.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script defer src="<?= base_url() ?>assets/js/script.js"></script>
  <script>
  const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
  });
  <?php
  if(!empty($this->session->flashdata('feedback'))){
      $feedback = $this->session->flashdata('feedback');
      $message = (isset($feedback['message'])) ? $feedback["message"] : "error";
      if(isset($feedback['status']) && $feedback["status"] == "success"){
      ?>
        Toast.fire({
          icon: 'success',
          title: '<?= $message ?>'
        });
      <?php
      }else{
        ?>
        Toast.fire({
          icon: 'error',
          title: '<?= $message ?>'
        });
        <?php
      }
   } ?>
  </script>
</body>
</html>
