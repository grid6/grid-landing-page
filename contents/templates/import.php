<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>GRID Fitness Hub - Import</title>

  <link rel="icon" type="image/ico" href="<?= base_url() ?>assets/new-logo.png"/>

  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
</head>
<body>
  <header>
    <section class="header-section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-10 col-md-11">
            <h5 class="m-0">Import Excel Data</h5>
          </div>
          <div class="col-2 col-md-1">
            <a href="#" class="d-flex align-items-center justify-content-end">
              <ion-icon name="log-out-outline" style="width:24px;height:24px;"></ion-icon>
            </a>
          </div>
        </div>
      </div>
    </section>
  </header>
  <main>
    <section class="history-section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <ul>
              <li>
                <h5 class="fw-normal">Import <?= ucwords($type) ?></h5>
                <form action="<?= base_url('Imports/'.$type); ?>" method="post" enctype="multipart/form-data">
                  <p class="fw-bold"></p>
                  <div class="d-flex align-items-center justify-content-between">
                    <small class="text-danger"><input name="fileExcel" type="file" /></small>
                    <button type="submit" class="btn btn-primary">Import</button>
                  </div>
                </form>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer class="fixed-bottom">
    <section>
      <div class="container p-0 border-top">
        <div class="d-flex align-items-center justify-content-between">
          <a href="javascript:void(0)" class="nav-bottom">Grid Fitness Hub &copy; 2023</a>

        </div>
      </div>
    </section>
  </footer>

  <script src="<?= base_url() ?>assets/js/jquery-3.5.1.min.js"></script>
  <script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
  <script type="module" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule="" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js"></script>
  <script defer src="<?= base_url() ?>assets/js/slick.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
  <script defer src="<?= base_url() ?>assets/js/script.js"></script>
</body>
</html>
