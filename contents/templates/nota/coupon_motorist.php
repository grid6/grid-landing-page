<!DOCTYPE html>
<html>
<head style="">
  <title style="">Pintap</title>
</head>

<!-- Periode voucher : Periode Promo : <?= (isset($promo->promo_start) && isset($promo->expired_at) && !empty($promo->promo_start) && !empty($promo->expired_at)) ? date("d F Y", strtotime($promo->promo_start))." - ".date("d F Y", strtotime($promo->expired_at)) : "-" ?> -->
<body style="margin: 0;font-size: 10px;font-weight: 400;line-height: 1.5;color: #212529;text-align: left;background-color: #fff;">
  <main style="display: block;">
    <div style="padding: 10px 10px;">
      <div style="position: relative;">
        <div style="padding: 5px 10px;width: 150px;border: 3px solid #000;float: right;">
          <div style="font-weight: bold;font-size: 10px;"> Nomor Kupon</div>
          <div style="color: #000;font-weight: bold;font-size: 18px;"><?= (isset($voucher->coupon_no)) ? $voucher->coupon_no : "-" ?></div>
        </div>
        <div>
          <table>
            <tr>
              <td>
                <div>
                  <img width="50" height="50" src="<?= base_url() ?>contents/assets/img/logo-baru-pintap.png" alt="">
                </div>
              </td>
              <td>
                <span>
                  <div style="font-size: 10px;font-weight: 200;">Promo Grosir (GROSIR - MOTORIS)</div>
                  <h3 style="margin-top: 5px;margin-bottom: 0;font-size: 14px;font-weight: bold;"><?= isset($grosir->name) ? $grosir->name : "-" ?></h3>
                  <address style="margin-top: 5px;font-size: 10px;font-style: normal;line-height: inherit;font-size: 10px;"><?= (isset($grosir->address) && !empty($grosir->address)) ? $grosir->address : "-" ?></address>
                </span>
              </td>
            </tr>
          </table>
        </div>
        <table style="margin-top: 30px;">
          <tr>
            <td style="vertical-align:top;">
              <div style="font-size: 10px;">Nama Motoris</div>
              <h4 style="padding-top: 10px;font-weight: bold;font-size: 14px;"><?= (isset($cust->first_name) && !empty($cust->first_name)) ? $cust->first_name : "-" ?></h4>
            </td>
            <td style="vertical-align:top;padding-left: 20px;">
              <div style="font-size: 10px;">Nomor Hp Motoris</div>
              <h4 style="padding-top: 10px;font-weight: bold;font-size: 14px;"><?= (isset($cust->phone) && !empty($cust->phone)) ? $cust->phone : "-" ?></h4>
            </td>
            <td style="vertical-align:top;padding-left: 20px;">
              <div style="font-size: 10px;">Tanggal Pemesanan</div>
              <h4 style="padding-top: 10px;font-weight: bold;font-size: 14px;"><?= date_id($voucher->created_at, "j F Y") ?></h4>
            </td>
            <td style="vertical-align:top;padding-left: 20px;">
              <div style="margin-bottom: .25rem;font-size: 10px;">Nama Grosir</div>
              <h4 style="margin-top: 0;margin-bottom: .5rem;font-weight: bold;font-size: 14px;"><?= (isset($grosir->name) && !empty($grosir->name)) ? $grosir->name : "-" ?></h4>
            </td>
            <td style="vertical-align:top;padding-left: 20px;">
              <div style="margin-bottom: .25rem;font-size: 10px;">Periode Promo</div>
              <h4 style="margin-top: 0;margin-bottom: .5rem;font-weight: bold;font-size: 14px;"><?= (isset($promo->start_date) && isset($promo->end_date) && !empty($promo->start_date) && !empty($promo->end_date)) ? date_id($promo->start_date, "j M Y", false)." - ".date_id($promo->end_date, "j M Y", false) : "-" ?></h4>
            </td>
          </tr>
        </table>
        <div style="margin-right: -8px;margin-left: -8px;margin-top: 10px;">
          <div style="position: relative;padding-right: 8px;padding-left: 8px;-ms-flex: 0 0 100%;flex: 0 0 100%;max-">
            <table style="border-collapse: collapse;width: 1200px;">
              <thead style="display:table-header-group;text-align:center;">
                <tr style="page-break-inside: avoid;">
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;">No.</td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;">Produk</td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;">Jumlah (Karton)</td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;">Harga Awal</td>
                  <td colspan="2" style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;">Total Promo MOTORIS</td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;">Promo/Harga Beli MOTORIS ke GROSIR</td>
                </tr>
              </thead>
              <tbody style="">
                <?php
                $noprd = 1;
                $tot_qty = $tot_sve = $tot_sub = 0;
                if(isset($voucher_product) && !empty($voucher_product)){
                    foreach($voucher_product as $promd){
                      $products = $this->product->find($promd->product_id);
                      $before_price = $promd->before_price;
                      $total_save = $promd->total_save;
                      $comp_percent = $promd->comp_percent;
                      $promdet = $this->promo->findProduct($promd->promo_product_id, "id, product_id, promo_id, price_comp as comp_price, comp_motorist, comp_grosir, comp_percent, price_motorist, price_buy, price_before, quantity, quantity_left, quantity_remain");
                      if(isset($promdet->id)){
  														$subgros = ($promdet->price_before * ($promdet->comp_grosir / 100)) * $promd->quantity;
  														// $before_price = $promd->before_price - $subgros;
  														$total_save = $promdet->price_motorist * $promd->quantity;
  														$comp_percent = $promdet->comp_motorist;
  										}
                      $subtotal = $promd->subtotal;
                      $voucherz = $this->voucher->find($promd->coupon_id);

                      $tot_qty += $promd->quantity;
                      $tot_sve += $total_save;
                      $tot_sub += $subtotal;
                ?>
                <tr style="page-break-inside: avoid;">
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;"><?= $noprd++ ?>.</td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;"><?= (isset($products->name)) ? $products->name : "-" ?></td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;"><?= $promd->quantity ?></td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;">Rp.<?= number_format($before_price, 0) ?></td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;font-weight: bold;font-size: 14px;"><?= $comp_percent ?>%</td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;font-weight: bold;font-size: 14px;">Rp.<?= number_format($total_save, 0) ?></td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;">Rp.<?= number_format($subtotal, 0) ?></td>
                </tr>
                <?php }
                } ?>
              </tbody>
              <tfoot style="">
                <tr style="page-break-inside: avoid;">
                  <td colspan="2" style="border: 0font-size: 14px;padding: 6px 6px;font-size: 14px;text-align: right;font-size: 14px;">Jumlah</td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;font-weight: bold;font-size: 14px;"><?= $tot_qty ?></td>
                  <td style="border: 0font-size: 14px;padding: 6px 6px;font-size: 14px;text-align: right;font-size: 14px;" colspan="2">Total</td>
                  <td style="border: 0;padding: 6px 6px;font-size: 14px;text-align: center;"><?= number_format($tot_sve, 0) ?></td>
                  <td style="border: 1px solid #E5E5E5;padding: 6px 6px;font-size: 14px;text-align: center;font-weight: bold;font-size: 14px;">Rp.<?= number_format($tot_sub, 0) ?></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div style="margin-right: -8px;margin-left: -8px;margin-top: 20px;width: 1000px;">
          <div style="position: relative;padding-right: 8px;padding-left: 8px;">
            <p style="margin-top: 0;margin-bottom: 1rem;orphans: 3;widows: 3;font-weight:bold;">Kententuan:</p>
            <ol style="margin-top: 0;margin-bottom: 1rem;padding-left: 1.5remfont-size: 10px;">
              <li style="padding-bottom: 0px;font-size: 10px;padding-left: 15px;">Pengambilan/Pembayaran barang sesuai ketersediaan barang di Grosir, dimana tidak ada ikatan bagi Grosir untuk menyimpan stok produk untuk Motoris selama ada pembeli lainnya.</li>
              <li style="padding-bottom: 0px;font-size: 10px;padding-left: 15px;">Pihak PINTAP tidak bertanggungjawab untuk pengambilan barang melebihi dari jumlah yang tertera diatas dan kualitas barang.</li>
              <li style="padding-bottom: 0px;font-size: 10px;padding-left: 15px;">Kupon ini hanya berlaku selama masa periode promo</li>
            </ol>
            <!-- <div style="float:right; width: 100px;margin-right: 100px;">
              <div style="padding-bottom: 3remfont-size: 10px;font-weight: bold;font-size: 10px; padding-bottom: 30px;"> Tanda tangan Grosir</div>
              <hr style="box-sizing: content-box;height: 0;overflow: visible;margin-top: 1rem;margin-bottom: 1rem;border-color: #000;">
            </div> -->
            <div style="font-size: 14pxfont-size: 10px; font-weight:bold;">Hubungi PINTAP melalui:</div>
            <?php if(isset($site_opt) && !empty($site_opt)){ ?>
            <?php if(isset($site_opt["telemarketing"]) && !empty($site_opt["telemarketing"])){ ?><div style="margin-top: 1remfont-size: 10px;font-size: 14pxfont-size: 10px;">Telemarketing <span style="font-weight: bold;font-size: 10px;"><?= $site_opt["telemarketing"] ?></span></div><?php } ?>
            <?php if(isset($site_opt["marketing_name"]) && isset($site_opt["marketing_phone"])){ ?><div style="font-size: 14pxfont-size: 10px;"><?= $site_opt["marketing_name"] ?> <span style="font-weight: bold;font-size: 10px;"><?= $site_opt["marketing_phone"] ?></span></div><?php } ?>
            <?php } ?>
          </div>
        </div>

      </div>
    </div>
  </main>
</body>
</html>
