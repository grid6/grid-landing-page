<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Forgot extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("form");
    }

    public function password(){
        $msg = $this->input->get("msg");
        $msg = urldecode($msg);
        $slug = $this->input->get("token");
        $slug = base64_decode($slug);
        $page = $this->db->get_where("users", ["email"=>$slug]);
        $name = "";
        $user = null;
        if($page && $page->num_rows() > 0){
            $pg = $page->row();
            $user = $pg;
            $name = $pg->first_name." ".$pg->last_name;
        }else{
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode(['code'=>404,'message'=>'user not found for this email : '.$slug]);
            exit();
        }

        $data = [
          'name'  => $name,
          'user'  => $user,
          'msg'   => $msg
        ];
        $this->load->view("forgot", $data);
    }

    public function done(){
        $this->load->view("done");
    }

    public function reset(){
        $slugs = $this->input->get("token");
        $slug = base64_decode($slugs);
        $usr = $this->db->get_where("users", ["email"=>$slug]);
        if(!$usr || ($usr && $usr->num_rows() == 0)){
            redirect(base_url("forgot/password?token=".$slugs));
        }
        $pass = $this->input->post("password");
        $cpass = $this->input->post("confirm_password");
        if($pass == $cpass){
            $hashpass = password_hash($pass, PASSWORD_BCRYPT, ['cost' => 10]);
            $this->db->update("users", ["password"=>$hashpass], ["email"=>$slug]);
            redirect(base_url("forgot/done"));
        }else{
          redirect(base_url("forgot/password?token=".$slugs."&msg=missmatch password and confirm password"));
        }
    }
}
