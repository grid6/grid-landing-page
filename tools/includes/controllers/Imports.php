<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Imports extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
        $this->load->helper("form");
        $this->load->library("excel");
        $this->load->model("ImportModel", "importz");
    }

    public function type($type="trainer"){
        $msg = $this->input->get("msg");
        $daz = $this->input->get("daz");
        $msg = urldecode($msg);
        $file = $this->input->post("file");
        // $page = $this->db->get_where("users", ["email"=>$slug]);
        $name = "";
        $user = null;


        $data = [
          'type'  => $type,
          'msg'   => $msg,
          'daz'   => $daz
        ];
        $this->load->view("import", $data);
    }

    public function save($type="trainer"){
        $type = strtolower($type);
        $slugs = explode("-", $this->input->post("qr"));
        $usr = (isset($slugs[2])) ? $slugs[2] : "";
        $schedule = (isset($slugs[1])) ? $slugs[1] : "";
        if(empty($usr) || empty($schedule)){
            redirect(base_url("scan/qr/$type?msg=format qr value wrong&daz=error"));
        }
        $usrs = $this->db->get_where("users", ["id"=>$usr]);
        if(!$usrs || ($usrs && $usrs->num_rows() == 0)){
            redirect(base_url("scan/qr/$type?msg=user not found&daz=error"));
        }

        if($type == "trainer"){
            $trainer_id = $slugs[0];
            $trainer = $this->db->get_where("trainers", ["id"=>$trainer_id]);
            if(!$trainer || ($trainer && $trainer->num_rows() == 0)){
                redirect(base_url("scan/qr/$type?msg=data trainer not found&daz=error"));
            }
            $sched = $this->db->get_where("trainer_schedules", ["id"=>$schedule]);
            if(!$sched || ($sched && $sched->num_rows() == 0)){
                redirect(base_url("scan/qr/$type?msg=schedule not found&daz=error"));
            }else{
               if($sched->row()->trainer_id != $trainer_id){
                 redirect(base_url("scan/qr/$type?msg=the schedule is not match with the trainer schedule&daz=error"));
               }
            }
            $whr = [
              "user_id"=>$usr,
              "trainer_id"=>$trainer_id,
              "schedule_id"=>$schedule
            ];
            $joined = $this->db->get_where("trainer_joined", $whr);
            if($joined && $joined->num_rows() > 0){
              if(empty($joined->row()->attended_at)){
                  $this->db->update("trainer_joined", ["attended_at"=>date("Y-m-d H:i:s")], ["id"=>$joined->row()->id]);
                  $name = $usrs->row()->first_name." ".$usrs->row()->last_name;
                  $traname = $trainer->row()->name;
                  redirect(base_url("scan/qr/$type?msg=attendance $type success, welcome $name to class $traname&daz=success"));
              }else{
                  redirect(base_url("scan/qr/$type?msg=you already attend this schedule&daz=error"));
              }
            }else{
                redirect(base_url("scan/qr/$type?msg=data $type not found&daz=error"));
            }
        }else if($type == "class" || $type == "course"){
          $course_id = $slugs[0];
          $course = $this->db->get_where("classes", ["id"=>$course_id]);
          if(!$course || ($course && $course->num_rows() == 0)){
              redirect(base_url("scan/qr/$type?msg=data class not found&daz=error"));
          }
          $sched = $this->db->get_where("classes_schedules", ["id"=>$schedule]);
          if(!$sched || ($sched && $sched->num_rows() == 0)){
              redirect(base_url("scan/qr/$type?msg=schedule not found&daz=error"));
          }else{
             if($sched->row()->class_id != $course_id){
               redirect(base_url("scan/qr/$type?msg=the schedule is not match with the class schedule&daz=error"));
             }
          }

          $joined = $this->db->get_where("classes_joined", ["user_id"=>$usr, "class_id"=>$course_id, "schedule_id"=>$schedule]);
          if($joined && $joined->num_rows() > 0){
            if(empty($joined->row()->attended_at)){
                $this->db->update("classes_joined", ["attended_at"=>date("Y-m-d H:i:s")], ["id"=>$joined->row()->id]);
                $name = $usrs->row()->first_name." ".$usrs->row()->last_name;
                $traname = $course->row()->name;
                redirect(base_url("scan/qr/$type?msg=attendance $type success, welcome $name to class $traname&daz=success"));
            }else{
                redirect(base_url("scan/qr/$type?msg=you already attend this schedule&daz=error"));
            }
          }else{
              redirect(base_url("scan/qr/$type?msg=data $type not found&daz=error"));
          }
        }else{
            redirect(base_url("scan/qr/$type?msg=data $type not found&daz=error"));
        }
    }

    public function users(){
  		if (isset($_FILES["fileExcel"]["name"])) {
  			$path = $_FILES["fileExcel"]["tmp_name"];
  			$object = PHPExcel_IOFactory::load($path);
  			foreach($object->getWorksheetIterator() as $worksheet)
  			{
  				$highestRow = $worksheet->getHighestRow();
  				$highestColumn = $worksheet->getHighestColumn();
  				for($row=2; $row<=$highestRow; $row++)
  				{
  					$first_name = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
  					$last_name = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
  					$email = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
  					$phone = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
  					$gender = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
  					$alamat = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
            if(!empty($email)){
                $sq=$this->db->select("id")->get_where("users", ["email"=>$email]);
                if($sq && $sq->num_rows() > 0){
            			$temp_data[] = array(
                      'id'=>$sq->row()->id,
          						'first_name'	=> $first_name,
          						'last_name'	=> $last_name,
          						'email'	=> $email,
                      'phone_code' => '62',
          						'phone'	=> $phone,
          						'gender'	=> $gender,
          						'address'	=> $alamat,
                      'password' => password_hash('ab123456', PASSWORD_BCRYPT, ['cost' => 10])
        					);
                }
            }
  				}
  			}
        $token = pos_login();
        if(!empty($token)){
  			// $insert = $this->importz->insert_user($temp_data);
  			// if($insert){
  				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-ok"></span> Data Berhasil di Import ke Database');
  				// redirect($_SERVER['HTTP_REFERER']);
          $transpos = pos_cust_multi($temp_data, $token);
          $this->session->set_flashdata('status', '<span class="glyphicon glyphicon-ok"></span> Data Berhasil di Import ke Database');
          // redirect($_SERVER['HTTP_REFERER']);
          echo 'sukses lur <br />';
          echo "<pre />";
          var_dump($transpos);

  			// }else{
  			// 	$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-remove"></span> Terjadi Kesalahan');
  			// 	// redirect($_SERVER['HTTP_REFERER']);
        //   echo 'gagal maning lur';
  			// }
        }
  		}else{
  			echo "Tidak ada file yang masuk";
  		}
	}

  public function convertDate($excel_date){
      $unix_date = ($excel_date - 25569) * 86400;
      $excel_date = 25569 + ($unix_date / 86400);
      $unix_date = ($excel_date - 25569) * 86400;
      return gmdate("Y-m-d", $unix_date);
  }

  public function contract(){
  		if (isset($_FILES["fileExcel"]["name"])) {
  			$path = $_FILES["fileExcel"]["tmp_name"];
  			$object = PHPExcel_IOFactory::load($path);
        $xact = $data_pos = [];
  			foreach($object->getWorksheetIterator() as $worksheet)
  			{
  				$highestRow = $worksheet->getHighestRow();
  				$highestColumn = $worksheet->getHighestColumn();
  				for($row=2; $row<=$highestRow; $row++)
  				{
  					$email = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
  					$kode_menu = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
  					$price = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
  					$tax = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
  					$paid_at = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
  					$active_date = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
  					$expire_date = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
  					$usrid=$plid='';
            if(!empty($email) && !empty($kode_menu)){
                $usr = $this->db->select("id")->get_where("users", ["email"=>$email]);
                if($usr && $usr->num_rows() > 0){
                    $usrid = $usr->row()->id;
                    $plan = $this->db->select("id, invoice_period, name")->get_where("plans", ["kode_menu"=>$kode_menu]);
                    if($plan && $plan->num_rows() > 0){
                      $data_pos[] = [
                          'id' => $usrid.$plan->row()->id.$row,
                          'kode_menu'=>$kode_menu,
                          'price' => $price,
                          'tax' => $tax,
                          'created_at' => date("Y-m-d H:i:s")
                      ];
                      $xact[] = array(
                          'tag' => 'main',
                          'user_id'	=> $usrid,
                          'name'  => $plan->row()->name,
                          'invoice_period' => $plan->row()->invoice_period,
                          'plan_id'	=> $plan->row()->id,
                          'price'	=> $price,
                          // 'paid_at'	=> $this->convertDate($paid_at),
                          'starts_at'	=> $this->convertDate($active_date),
                          'ends_at'	=> $this->convertDate($expire_date),
                      );
                      $paymentdata[] = array(
                          'user_id'	=> $usrid,
                          'payable_type' => 'Xtwoend\HySubscribe\Model\Plan',
                          'payable_id' => $plan->row()->id,
                          'invoice_period' => $plan->row()->invoice_period,
                          'plan_id'	=> $plan->row()->id,
                          'price'	=> $price,
                          // 'paid_at'	=> $this->convertDate($paid_at),
                          'starts_at'	=> $this->convertDate($active_date),
                          'ends_at'	=> $this->convertDate($expire_date),
                      );
                    }
      				  }
            }
  				}
  			}
        // print_r($xact);
        // exit();
        $token = pos_login();
        if(!empty($token)){
          // echo "<pre />";
          // var_dump($data_pos);
          // exit();
            $insert = $this->importz->insert_contract($xact);
            $insert = $this->importz->insert_trans($xact);
    			// if($insert){
            $transpos = pos_trans($data_pos, $token);
    				$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-ok"></span> Data Berhasil di Import ke Database');
    				// redirect($_SERVER['HTTP_REFERER']);
            echo 'sukses lur <br />';
            echo "<pre />";
            var_dump($transpos);
    			// }else{
    			// 	$this->session->set_flashdata('status', '<span class="glyphicon glyphicon-remove"></span> Terjadi Kesalahan');
    			// 	// redirect($_SERVER['HTTP_REFERER']);
          //   echo 'gagal maning lur';
    			// }
        }else{
            echo "gagal login bosku";
        }

  		}else{
  			echo "Tidak ada file yang masuk";
  		}
	}
}
