<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Claim extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper("form");
        $this->sess = $this->session->userdata("users");
        $sess = $this->sess;
        $this->user = isset($sess["user_id"]) ? $sess["user_id"] : 0;
        if(empty($this->user)){
            $this->session->sess_destroy();
            $this->session->set_flashdata('feedback', array('message'=>"your session has ended", 'error'=>true, 'status'=>'error'));
            redirect(site_url("login"));
        }
    }

    public function index(){
        $this->load->view("scan");
    }

    public function scan(){
        $code = $this->input->post("code");
        $err = "";
        $data = [];
        if(!empty($code)){
            $tick = $this->db->get_where("transticket", ['code'=>$code]);
            if($tick && $tick->num_rows() > 0){
                $ticc = $tick->row();
                if($ticc->status > 0){
                    $err = "this ticket has been claimed";
                }else{
                    $prod = $this->db->get_where("produk", ['id'=>$ticc->idproduk]);
                    if($prod && $prod->num_rows() > 0){
                        $data["produk"] = $prod->row()->nama;
                    }else $data["produk"] = "Tiket $code";
                    $uptik = [
                        "status"=>1,
                        'scanned_by'=>$this->user,
                        'scanned_at'=>date("Y-m-d H:i:s")
                    ];
                    $this->db->update("transticket", $uptik, ["id"=>$ticc->id]);
                    $ttlog = [
                        'ticket'=>$code,
                        'tgl'=>date("Y-m-d H:i:s"),
                        'status'=>1,
                        'created_by'=>$this->user
                    ];
                    $this->db->insert("ticketlog", $ttlog);
                }
            }else $err = "ticket not found";
        }else{
            $err = "ticket code is required";
        }
        $output = [
            "status"=>(!empty($data)) ? "success" : "error",
            "message"=>$err,
        ];
        if(!empty($data)) $output["data"] = $data;
        echo json_encode($output);
    }

    public function history(){
        $clam = $this->db->get_where("ticketlog", ["created_by"=>$this->user]);
        $claims = ($clam && $clam->num_rows() > 0) ? $clam->result() : [];
        $data_view = [
            'claim'=>$claims
        ];
        $this->load->view("claim", $data_view);
    }
}
