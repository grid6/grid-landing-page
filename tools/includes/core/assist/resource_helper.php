<?php
/**
 * @author   Abyan Ahmad fathin <abyan.site@gmail.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('resourceSales')){
		function resourceSales($arr){
				$ci =& get_instance();
				if(!isset($ci->warung)) $ci->load->model("M_Warung", "M_Warung");
				$data = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								/* checking price_before*/
								if(isset($ar->price)){
											$ar->price = (double)$ar->price;
								}
								/* checking price_buy*/
								if(isset($ar->qty)){
											$ar->qty = (double)$ar->qty;
								}
								/* checking price_buy*/
								if(isset($ar->subtotal)){
											$ar->subtotal = (double)$ar->subtotal;
											unset($ar->subtotal);
								}

								$daprom = null;
								$avprod = 0;
								if(isset($ar->warung_id)){
													$gros = $ci->warung->find($ar->warung_id);
													if(isset($gros->id)){
															$daprom = [
																	'id'=>$gros->id,
																	'name'=>$gros->name,
																	'owner'=>$gros->owner
															];
													}
								}

								$daprod = null;
								if(isset($ar->product_id)){
											$prosql = $ci->db->select("id, name, pcs_image as image")->get_where("products", ["id"=>$ar->product_id]);
											if($prosql && $prosql->num_rows() > 0){
													$prow = $prosql->row();
													$pcs_image = (!empty($prow->image)) ? $prow->image : 'https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg';
													if(strpos($pcs_image, 'http') !== false){
															$pcs_image = $pcs_image;
													}else $pcs_image = 'https://'.$pcs_image;
													$daprod = [
															'id'=>$ar->product_id,
															'name'=>$prow->name,
															'image'=>$pcs_image,
													];
											}
								}

								if(isset($daprom["id"])){
									$ar->warung = $daprom;
									if(isset($ar->warung_id)) unset($ar->warung_id);
								}

								if(isset($daprod["id"])){
									$ar->product = $daprod;
									if(isset($ar->product_id)) unset($ar->product_id);
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourceProduct')){
		function resourceProduct($arr, $lists = true){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								/* checking price_before*/
								if(isset($ar->price_before)){
											$ar->price_before = (double)$ar->price_before;
											if(isset($ar->comp_grosir)){
													$ar->price_before = $ar->price_before - ($ar->price_before * ($ar->comp_grosir/100));
													unset($ar->comp_grosir);
											}
								}
								/* checking price_buy*/
								if(isset($ar->price_buy)){
											$ar->price_buy = (double)$ar->price_buy;
								}
								/* checking price_buy*/
								$stock=0;
								if(isset($ar->quantity_left)){
											$stock = (double)$ar->quantity_left;
											unset($ar->quantity_left);
								}
								$ar->stock = $stock;
								$ar->stock_status = (!empty($stock)) ? "available" : "empty";


								if(isset($ar->quantity_remain)) unset($ar->quantity_remain);

								$daprom = null;
								$avprod = 0;
								if(isset($ar->promo_id)){
										$prosql = $ci->promo->find(["id"=>$ar->promo_id], "id, code, grosir_id, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date");
										if(isset($prosql->id)){
												$prom = $prosql;
												$gros = $ci->grosir->find($prom->grosir_id);
												$daprom = [
														'id'=>$ar->promo_id,
														'code'=>$prom->code,
														'start'=>$prom->start_date,
														'end'=>$prom->end_date,
												];
												$grozz = null;
												if(isset($gros->id)){
														$grozz = [
																'id'=>$gros->id,
																'name'=>$gros->name,
																'area'=>$gros->area,
																'latitude'=>$gros->latitude,
																'longitude'=>$gros->longitude,
																'address'=>(string)$gros->address
														];
														$totsql = $ci->promo->total_productgros("id = '".$ar->id."' AND promo_id IN(SELECT id FROM promos WHERE status = 'ACTIVE')");
														$avprod = (!empty($totsql)) ? (double)$totsql : 0;
												}
												$daprom['grosir'] = $grozz;
										}
								}else if(isset($ar->id_promo) && empty($daprom)){
											$prosql = $ci->promo->find(["id"=>$ar->id_promo], "id, code, grosir_id, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date");
											if(isset($prosql->id)){
													$prom = $prosql;
													$daprom = [
															'id'=>$ar->id_promo,
															'code'=>$prom->code,
															'start'=>$prom->start_date,
															'end'=>$prom->end_date,
													];
													if(!$lists){
															$gros = $ci->grosir->find($prom->grosir_id, "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service");
															$grozz = null;
															if(isset($gros->id)){
																	$grozz = singleGrosirOnly($gros);
															}
															$daprom['grosir'] = $grozz;
													}
											}
											unset($ar->id_promo);
								}

								$daprod = null;
								if(isset($ar->product_id)){
										$prosql = $ci->product->findCond(["id"=>$ar->product_id], "id, code, name, pcs_image as image, brand_id, packtype_id, qty, IFNULL(name_to_display, '') as name_to_display");
										$daprod = resourcesingleProductOnly($prosql, "", "", false);
										// if(isset($prosql->id)){
										// 		$prow = $prosql;
										//
										// 		$pcs_image = (!empty($prow->image)) ? $prow->image : 'https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg';
										// 		if(strpos($pcs_image, 'http') !== false){
										// 				$pcs_image = $pcs_image;
										// 		}else $pcs_image = 'https://'.$pcs_image;
										//
										// 		$daprod = [
										// 				'id'=>$ar->product_id,
										// 				'name'=>$prow->name,
										// 				'image'=>$pcs_image,
										// 		];
										//
										// }
								}

								$ar->terkait = $avprod;

								if(isset($daprom["id"])){
									$ar->promo = $daprom;
									if(isset($ar->promo_id)) unset($ar->promo_id);
								}

								if(isset($daprod->id)){
									$ar->product = $daprod;
									if(isset($ar->product_id)) unset($ar->product_id);
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourceProductOnly')){
		function resourceProductOnly($arr, $city_code='', $area_service=''){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								if(isset($ar->total_buy)) $ar->total_buy = (int)$ar->total_buy;
								$deskripsi = '';
								$ar->qty = (!empty($ar->qty)) ? (double)$ar->qty : 1;
								/* fill image*/
								if(isset($ar->image)){
										$pcs_image = (!empty($ar->image)) ? $ar->image : 'https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg';
										if(strpos($pcs_image, 'http') !== false){
												$pcs_image = $pcs_image;
										}else $pcs_image = 'https://'.$pcs_image;
										$ar->image = $pcs_image;
								}
								/* fill brand*/
								if(isset($ar->brand_id)){
										$brands = $ci->product->findBrandByCond(['id'=>$ar->brand_id], 'id, code, name, principalId, category1Id, category2Id, category3Id, category4Id');
										$ar->brand = singleBrands($brands);
										unset($ar->brand_id);
								}
								/* fill pack*/
								if(isset($ar->packtype_id)){
										$packs = $ci->product->findPackTypeByCond(['id'=>$ar->packtype_id], 'id, code, name');
										$ar->packtype = $packs;
										unset($ar->packtype_id);
								}
								if(isset($ar->name)) $deskripsi .= 'SKU Name : '.$ar->name;
								if(isset($ar->brand->name)) $deskripsi .= '\nBrand : '.$ar->brand->name;
								if(isset($ar->brand->principal->name)) $deskripsi .= '\nPrincipal : '.$ar->brand->principal->name;
								if(isset($ar->brand->category) && count($ar->brand->category) > 0) {
										$catz = [];
										foreach($ar->brand->category as $ckz=>$ctz){
												if(isset($ctz->name)){
														$catz[] = $ctz->name;
												}
										}
										$catzz = implode(" / ", $catz);
										$deskripsi .= '\nCategory : '.$catzz;
								}
								if(isset($ar->qty) && !empty($ar->qty)) {
										$deskripsi .= '\nTotal Qty in Pouch : '.$ar->qty.' Pouch';
								}
								$whereLoc = "";
								if(!empty($area_service) || !empty($city_code)){
										if(!empty($area_service) && empty($city_code)){
												$whereLoc = " AND grosir_id IN(SELECT id FROM grosirs WHERE area_service_id='$area_service')";
										}else if(empty($area_service) && !empty($city_code)){
												$whereLoc = " AND grosir_id IN(SELECT id FROM grosirs WHERE city_code='$city_code')";
										}else{
												$whereLoc = " AND grosir_id IN(SELECT id FROM grosirs WHERE city_code='$city_code' OR area_service_id='$area_service')";
										}
								}
								// $whereArea = (!empty($area_service)) ? " OR area_service_id = '$area_service'" : "";
								// $whereCity = (!empty($city_code)) ? " AND grosir_id IN(SELECT id FROM grosirs WHERE city_code = '$city_code'$whereLoc)" : "";
								$totsql = $ci->promo->total_productgros("product_id = '".$ar->id."' AND quantity_left > 0 AND promos.status = 'ACTIVE'$whereLoc");
								$avprod = (!empty($totsql)) ? (double)$totsql : 0;
								$ar->terkait = $avprod;
								$ar->description = $deskripsi;

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourcesingleProductOnly')){
		function resourcesingleProductOnly($ar, $city_code='', $area_service='', $without_promo=true){
				$ci =& get_instance();
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = [];
				if(!empty($ar)){
								$deskripsi = '';
								$ar->qty = (!empty($ar->qty)) ? (double)$ar->qty : 1;
								/* fill image*/
								if(isset($ar->image)){
										$pcs_image = (!empty($ar->image)) ? $ar->image : 'https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg';
										if(strpos($pcs_image, 'http') !== false){
												$pcs_image = $pcs_image;
										}else $pcs_image = 'https://'.$pcs_image;
										$ar->image = $pcs_image;
								}
								/* fill brand*/
								if(isset($ar->brand_id)){
										$brands = $ci->product->findBrandByCond(['id'=>$ar->brand_id], 'id, code, name, principalId, category1Id, category2Id, category3Id, category4Id');
										$ar->brand = singleBrands($brands);
										unset($ar->brand_id);
								}
								/* fill pack*/
								if(isset($ar->packtype_id)){
										$packs = $ci->product->findPackTypeByCond(['id'=>$ar->packtype_id], 'id, code, name');
										$ar->packtype = $packs;
										unset($ar->packtype_id);
								}
								if(isset($ar->name)) $deskripsi .= 'SKU Name : '.$ar->name;
								if(isset($ar->brand->name)) $deskripsi .= '\nBrand : '.$ar->brand->name;
								if(isset($ar->brand->principal->name)) $deskripsi .= '\nPrincipal : '.$ar->brand->principal->name;
								if(isset($ar->brand->category) && count($ar->brand->category) > 0) {
										$catz = [];
										foreach($ar->brand->category as $ckz=>$ctz){
												if(isset($ctz->name)){
														$catz[] = $ctz->name;
												}
										}
										$catzz = implode(" / ", $catz);
										$deskripsi .= '\nCategory : '.$catzz;
								}
								if(isset($ar->qty) && !empty($ar->qty)) {
										$deskripsi .= '\nTotal Qty in Pouch : '.$ar->qty.' Pouch';
								}

								if($without_promo){
										$whereLoc = "";
										if(!empty($area_service) || !empty($city_code)){
												if(!empty($area_service) && empty($city_code)){
														$whereLoc = " AND grosir_id IN(SELECT id FROM grosirs WHERE area_service_id='$area_service')";
												}else if(empty($area_service) && !empty($city_code)){
														$whereLoc = " AND grosir_id IN(SELECT id FROM grosirs WHERE city_code='$city_code')";
												}else{
														$whereLoc = " AND grosir_id IN(SELECT id FROM grosirs WHERE city_code='$city_code' OR area_service_id='$area_service')";
												}
										}
										// $whereArea = (!empty($area_service)) ? " OR area_service_id = '$area_service'" : "";
										// $whereCity = (!empty($city_code)) ? " AND grosir_id IN(SELECT id FROM grosirs WHERE city_code = '$city_code'$whereLoc)" : "";
										$totsql = $ci->promo->list_productgros("quantity_left > 0 AND product_id = '".$ar->id."' AND promos.status = 'ACTIVE'$whereLoc");
										$avprod = (!empty($totsql)) ? resourceProduct($totsql, false) : [];

										$ar->terkait = $avprod;
								}
								$ar->description = $deskripsi;

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourceProductsingle')){
		function resourceProductsingle($ar){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = [];
				if(!empty($ar)){
								/* checking price_before*/
								if(isset($ar->price_before)){
											$ar->price_before = (double)$ar->price_before;
								}
								/* checking price_buy*/
								if(isset($ar->price_buy)){
											$ar->price_buy = (double)$ar->price_buy;
								}
								/* checking price_buy*/
								if(isset($ar->quantity_left)){
											$ar->stock = (double)$ar->quantity_left;
											unset($ar->quantity_left);
								}

								$daprom = null;
								$avprod = null;
								if(isset($ar->promo_id)){
											$prosql = $ci->promo->find(["id"=>$ar->promo_id], "id, code, grosir_id, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date");
											if(isset($prosql->id)){
													$prom = $prosql;
													$gros = $ci->grosir->find($prom->grosir_id);
													$daprom = [
															'id'=>$ar->promo_id,
															'code'=>$prom->code,
															'start'=>$prom->start_date,
															'end'=>$prom->end_date,
													];
													$grozz = null;
													if(isset($gros->id)){
															$grozz = [
																	'id'=>$gros->id,
																	'name'=>$gros->name,
																	'area'=>$gros->area,
																	'latitude'=>$gros->latitude,
																	'longitude'=>$gros->longitude,
																	'address'=>(string)$gros->address
															];
															$totsql = $ci->promo->list_productgros("product_id = '".$ar->product_id."' AND promo_id IN(SELECT id FROM promos WHERE grosir_id IN(SELECT id FROM grosirs WHERE city_code = '".$gros->city_code."' OR area_service_id = '".$gros->area_service_id."') AND status = 'ACTIVE')", "all");
															$avprod = (!empty($totsql)) ? resourceProduct($totsql, false) : [];
													}
													$daprom['grosir'] = $grozz;
											}
								}

								$daprod = null;
								if(isset($ar->product_id)){
											$prosql = $ci->product->findCond(["id"=>$ar->product_id], "id, name, name_to_display, pcs_image as image, packtype_id, brand_id");
											if(isset($prosql->id)){
													$prow = singleProductOnly($prosql);
													$ar->product = $prow;
													unset($ar->product_id);
											}
								}

								$ar->terkait = $avprod;

								if(isset($daprom["id"])){
									$ar->promo = $daprom;
									if(isset($ar->promo_id)) unset($ar->promo_id);
								}

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourceProductCart')){
		function resourceProductCart($ar){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = [];
				if(!empty($ar)){
								/* checking price_before*/
								if(isset($ar->price_before)){
											$ar->price_before = (double)$ar->price_before;
											if(isset($ar->comp_grosir)){
													$ar->price_before = $ar->price_before - ($ar->price_before * ($ar->comp_grosir/100));
													unset($ar->comp_grosir);
											}
								}
								/* checking price_buy*/
								if(isset($ar->price_buy)){
											$ar->price_buy = (double)$ar->price_buy;
								}
								/* checking price_save*/
								if(isset($ar->price_save)){
											$ar->price_save = (double)$ar->price_save;
								}
								/* checking qty_left*/
								if(isset($ar->quantity_left)){
											$ar->stock = (double)$ar->quantity_left;
											unset($ar->quantity_left);
								}
								/* checking quantity*/
								if(isset($ar->quantity)){
											$ar->qty_promo = (double)$ar->quantity;
											if(isset($ar->quantity_remain)){
													$qtpromo = $ar->qty_promo - $ar->quantity_remain;
													$ar->qty_promo = ($qtpromo > 0) ? $qtpromo : 0;
													unset($ar->quantity_remain);
											}
											unset($ar->quantity);
								}

								$daprom = null;
								$avprod = null;
								if(isset($ar->promo_id)){
											$prosql = $ci->promo->find(["id"=>$ar->promo_id], "id, code, grosir_id, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date");
											if(isset($prosql->id)){
													$prom = $prosql;
													$gros = $ci->grosir->find($prom->grosir_id);
													$daprom = [
															'id'=>$ar->promo_id,
															'code'=>$prom->code,
															'start'=>$prom->start_date,
															'end'=>$prom->end_date,
													];
											}
								}

								$daprod = null;
								if(isset($ar->product_id)){
											$prosql = $ci->product->findCond(["id"=>$ar->product_id], "id, name, pcs_image as image");
											if(isset($prosql->id)){
													$prow = $prosql;
													$pcs_image = (!empty($prow->image)) ? $prow->image : 'https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg';
													if(strpos($pcs_image, 'http') !== false){
															$pcs_image = $pcs_image;
													}else $pcs_image = 'https://'.$pcs_image;
													$daprod = [
															'id'=>$ar->product_id,
															'name'=>$prow->name,
															'image'=>$pcs_image,
													];
											}
								}


								if(isset($daprom["id"])){
									$ar->promo = $daprom;
									if(isset($ar->promo_id)) unset($ar->promo_id);
								}

								if(isset($daprod["id"])){
									$ar->product = $daprod;
									if(isset($ar->product_id)) unset($ar->product_id);
								}

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourcePromo')){
		function resourcePromo($arr){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				$data = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								/* checking price_before*/
								if(isset($ar->budget)){
											$ar->budget = (double)$ar->budget;
								}
								/* checking note*/
								if(isset($ar->note)){
											$ar->note = json_decode($ar->note);
								}
								/* checking note mgr*/
								if(isset($ar->note_manager)){
											$ar->note_manager = json_decode($ar->note_manager);
								}
								/* checking note mgr*/
								if(isset($ar->note_grosir)){
											$ar->note_grosir = json_decode($ar->note_grosir);
								}
								$dagros = null;
								if(isset($ar->grosir_id)){
											$prosql = $ci->grosir->find($ar->grosir_id, "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service");
											if(isset($prosql->id)){
													$ar->grosir = singleGrosirOnly($prosql);
											}
											unset($ar->grosir_id);
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourcePromoWithProduct')){
		function resourcePromoWithProduct($arr, $dtl=false){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								/* checking price_before*/
								if(isset($ar->rank)){
										unset($ar->rank);
								}

								if(isset($ar->budget)){
											$ar->budget = (double)$ar->budget;
								}
								/* checking note*/
								if(isset($ar->note)){
											$ar->note = json_decode($ar->note);
								}
								/* checking note mgr*/
								if(isset($ar->note_manager)){
											$ar->note_manager = json_decode($ar->note_manager);
								}
								/* checking note mgr*/
								if(isset($ar->note_grosir)){
											$ar->note_grosir = json_decode($ar->note_grosir);
								}
								$dagros = null;
								if(isset($ar->grosir_id)){
											$prosql = $ci->grosir->find($ar->grosir_id, "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service");
											if(isset($prosql->id)){
													$ar->grosir = singleGrosirOnly($prosql);
											}
											unset($ar->grosir_id);
								}

								$list_product = $ci->promo->list_productgros(["promo_id"=>$ar->id], 20, 1);
								$ar->product = (!empty($list_product)) ? resourceProduct($list_product) : [];


								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourcePromosingle')){
		function resourcePromosingle($ar){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				$data = [];
				if(!empty($ar)){
								/* checking price_before*/
								if(isset($ar->budget)){
											$ar->budget = (double)$ar->budget;
								}
								/* checking note*/
								if(isset($ar->note)){
											$ar->note = json_decode($ar->note);
								}
								/* checking note mgr*/
								if(isset($ar->note_manager)){
											$ar->note_manager = json_decode($ar->note_manager);
								}
								/* checking note mgr*/
								if(isset($ar->note_grosir)){
											$ar->note_grosir = json_decode($ar->note_grosir);
								}
								$dagros = null;
								if(isset($ar->grosir_id)){
											$prosql = $ci->grosir->find($ar->grosir_id, "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service");
											if(isset($prosql->id)){
													$ar->grosir = singleGrosirOnly($prosql);
											}
											unset($ar->grosir_id);
								}

								$data[] = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourceGrosir')){
		function resourceGrosir($arr, $longlat=''){
				$ci =& get_instance();
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				if(!isset($ci->region)) $ci->load->model("M_Region", "region");
				$data = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								/* checking price_before*/
								/* fill area*/
								if(isset($ar->area_service) && !empty($ar->area_service)){
										$areas = $ci->region->findAreaCond(['id'=>$ar->area_service], 'id, name');
										$ar->area_service = $areas;
								}
								/* fill subdistrict_id*/
								if(isset($ar->subdistrict_id) && !empty($ar->subdistrict_id)){
										$sdists = $ci->region->findSubdistCond(['subcounty_code'=>$ar->subdistrict_id], 'subcounty_code as id, subcounty_name as name, IFNULL(zip_code, "") as zip_code');
										$ar->subdistrict = $sdists;
										unset($ar->subdistrict_id);
								}
								/* avatar grosir*/
								if(isset($ar->avatar) && !empty($ar->avatar)){
										$aravat = (isset($ar->avatar) && !empty($ar->avatar) && strpos($ar->avatar, 'http') !== false) ? $ar->avatar : "https://".$ar->avatar;
								}else $aravat = base_url('contents/assets/img/grosir_def.jpg');
								$ar->avatar = $aravat;
								/* banner grosir*/
								if(!isset($ar->photos)){
										$grosBan = $ci->grosir->getBanner(['grosir_id'=>$ar->id], 'banner_image as image');
										if(!empty($grosBan)){
												$daphot = [];
												foreach($grosBan as $kgab=>$gban){
														$defban = (strpos($gban["image"], 'http') !== false) ? $gban["image"] : "https://".$gban["image"];
														$daphot[] = ['image'=>$defban, 'default'=>($kgab == 0) ? true : false];
												}
												if(count($daphot) < 4){
													$totdap = 4 - count($daphot);
													$daphot2 = [];
													for($iphot=0;$iphot<$totdap;$iphot++){
															$daphot2[] = ['image'=> base_url('contents/assets/img/grosir_banner_def_1.jpg'), 'default'=>false];
													}
													$daphot = array_merge($daphot, $daphot2);
												}
												$ar->photos = $daphot;
										}else{
												$aravat = "";
												if(isset($ar->avatar) && !empty($ar->avatar)){
														$aravat = (isset($ar->avatar) && !empty($ar->avatar) && strpos($ar->avatar, 'http') !== false) ? $ar->avatar : "https://".$ar->avatar;
												}

												$grosav = (!empty($aravat)) ? $aravat : base_url('contents/assets/img/grosir_def.jpg');
												$ar->photos = [
														['image'=> $grosav, 'default'=>true],
														['image'=> base_url('contents/assets/img/grosir_banner_def_1.jpg'), 'default'=>false],
														['image'=> base_url('contents/assets/img/grosir_banner_def_2.jpg'), 'default'=>false],
														['image'=> base_url('contents/assets/img/grosir_banner_def_3.jpg'), 'default'=>false]
												];
										}
								}

								if(isset($ar->distance) && !empty($ar->distance)){
										$ar->distance = $ar->distance." km";
								}else{
										$distance = "";
										$latlong = $longlat;
										if(!empty($latlong)){
												$lalo = explode(",", $latlong);
												$latitude = $lalo[0];
												$longitude = (isset($lalo[1])) ? $lalo[1] : '';
												if(!empty($longitude) && !empty($ar->longitude) && !empty($ar->latitude)){
														$distance = (!empty($latitude) && !empty($longitude) && !empty($ar->latitude) && !empty($ar->longitude)) ? calculate_distance($latitude, $longitude, $ar->latitude, $ar->longitude)." km" : '';
												}
										}
										$ar->distance = $distance;
								}

								if(isset($ar->district_id) && !empty($ar->district_id)){
										$dists = $ci->region->findDistCond(['county_code'=>$ar->district_id], 'county_code as id, county_name as name');
										$ar->district = $dists;
										unset($ar->district_id);
								}
								/* fill city_id*/
								if(isset($ar->city_id) && !empty($ar->city_id)){
										$cits = $ci->region->findCityCond(['city_code'=>$ar->city_id], 'city_code as id, city_name as name');
										$ar->city = $cits;
										unset($ar->city_id);
								}
								/* fill prov_id*/
								if(isset($ar->province_id) && !empty($ar->province_id)){
										$provs = $ci->region->findProvCond(['province_code'=>$ar->province_id], 'province_code as id, name');
										$ar->province = $provs;
										unset($ar->province_id);
								}

								$tot_promo = $ci->promo->total_productgros("promo_id IN(SELECT id FROM promos WHERE grosir_id  = '".$ar->id."' AND status = 'ACTIVE')");
								$ar->total_product = (!empty($tot_promo)) ? (int)$tot_promo : 0;
								$promo = $ci->promo->list_productgros("promo_id IN(SELECT id FROM promos WHERE grosir_id  = '".$ar->id."' AND status = 'ACTIVE')", 6, 1);

								if(!empty($promo)){
										$ar->product = resourceProduct($promo);
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourceGrosirsingle')){
		function resourceGrosirsingle($ar, $rpp=6, $page=1){
				$ci =& get_instance();
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				if(!isset($ci->region)) $ci->load->model("M_Region", "region");
				$data = [];
				if(!empty($ar)){
								/* fill area*/
								if(isset($ar->area_service) && !empty($ar->area_service)){
										$areas = $ci->region->findAreaCond(['id'=>$ar->area_service], 'id, name');
										$ar->area_service = $areas;
								}
								/* fill subdistrict_id*/
								if(isset($ar->subdistrict_id) && !empty($ar->subdistrict_id)){
										$sdists = $ci->region->findSubdistCond(['subcounty_code'=>$ar->subdistrict_id], 'subcounty_code as id, subcounty_name as name, IFNULL(zip_code, "") as zip_code');
										$ar->subdistrict = $sdists;
										unset($ar->subdistrict_id);
								}
								/* avatar grosir*/
								if(isset($ar->avatar) && !empty($ar->avatar)){
										$aravat = (isset($ar->avatar) && !empty($ar->avatar) && strpos($ar->avatar, 'http') !== false) ? $ar->avatar : "https://".$ar->avatar;
								}else $aravat = base_url('contents/assets/img/grosir_def.jpg');
								$ar->avatar = $aravat;
								/* avatar grosir*/
								if(!isset($ar->photos)){
										$daphot = [];
										$grosBan = $ci->grosir->getBanner(['grosir_id'=>$ar->id], 'banner_image as image');
										if(!empty($grosBan)){
												foreach($grosBan as $kgab=>$gban){
														$defban = (strpos($gban["image"], 'http') !== false) ? $gban["image"] : "https://".$gban["image"];
														$daphot[] = ['image'=>$defban, 'default'=>($kgab == 0) ? true : false];
												}
										}
										$ar->photos = $daphot;
								}

								if(isset($ar->district_id) && !empty($ar->district_id)){
										$dists = $ci->region->findDistCond(['county_code'=>$ar->district_id], 'county_code as id, county_name as name');
										$ar->district = $dists;
										unset($ar->district_id);
								}
								/* fill city_id*/
								if(isset($ar->city_id) && !empty($ar->city_id)){
										$cits = $ci->region->findCityCond(['city_code'=>$ar->city_id], 'city_code as id, city_name as name');
										$ar->city = $cits;
										unset($ar->city_id);
								}
								/* fill prov_id*/
								if(isset($ar->province_id) && !empty($ar->province_id)){
										$provs = $ci->region->findProvCond(['province_code'=>$ar->province_id], 'province_code as id, name');
										$ar->province = $provs;
										unset($ar->province_id);
								}
								/* checking price_before*/
								// $promo = $ci->promo->listordgros("grosir_id = '".$ar->id."'", $rpp, $page);
								$promo = $ci->promo->lists(["grosir_id"=>$ar->id, "status"=>"ACTIVE"], $rpp, $page);
								if(!empty($promo)){
										$ar->promos = resourcePromoWithProduct($promo);
								}

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourceOnlyGrosir')){
		function resourceOnlyGrosir($arr, $longlat=''){
				$ci =& get_instance();
				if(!isset($ci->region)) $ci->load->model("M_Region", "region");
				$data = [];
				if(!empty($arr)){
							foreach($arr as $ar){
								/* fill area*/
								if(isset($ar->area_service) && !empty($ar->area_service)){
										$areas = $ci->region->findAreaCond(['id'=>$ar->area_service], 'id, name');
										$ar->area_service = $areas;
								}
								/* avatar grosir*/
								if(isset($ar->avatar) && !empty($ar->avatar)){
										$aravat = (isset($ar->avatar) && !empty($ar->avatar) && strpos($ar->avatar, 'http') !== false) ? $ar->avatar : "https://".$ar->avatar;
								}else $aravat = base_url('contents/assets/img/grosir_def.jpg');
								$ar->avatar = $aravat;
								/* banner grosir*/
								if(!isset($ar->photos)){
											$grosBan = $ci->grosir->getBanner(['grosir_id'=>$ar->id], 'banner_image as image');
											if(!empty($grosBan)){
														$daphot = [];
														foreach($grosBan as $kgab=>$gban){
																$defban = (strpos($gban["image"], 'http') !== false) ? $gban["image"] : "https://".$gban["image"];
																$daphot[] = ['image'=>$defban, 'default'=>($kgab == 0) ? true : false];
														}
														if(count($daphot) < 4){
															$totdap = 4 - count($daphot);
															$daphot2 = [];
															for($iphot=0;$iphot<$totdap;$iphot++){
																	$daphot2[] = ['image'=> base_url('contents/assets/img/grosir_banner_def_1.jpg'), 'default'=>false];
															}
															$daphot = array_merge($daphot, $daphot2);
														}
														$ar->photos = $daphot;
											}else{
													$ar->photos = [
															['image'=> base_url('contents/assets/img/grosir_def.jpg'), 'default'=>true],
															['image'=> base_url('contents/assets/img/grosir_banner_def_1.jpg'), 'default'=>false],
															['image'=> base_url('contents/assets/img/grosir_banner_def_2.jpg'), 'default'=>false],
															['image'=> base_url('contents/assets/img/grosir_banner_def_3.jpg'), 'default'=>false]
													];
											}
								}
								/* fill distance*/
								if(isset($ar->distance) && !empty($ar->distance)){
										$ar->distance = $ar->distance." km";
								}else{
										$distance = "";
										$latlong = $longlat;
										if(!empty($latlong)){
												$lalo = explode(",", $latlong);
												$latitude = $lalo[0];
												$longitude = (isset($lalo[1])) ? $lalo[1] : '';
												if(!empty($longitude) && !empty($ar->longitude) && !empty($ar->latitude)){
														$distance = (!empty($latitude) && !empty($longitude) && !empty($ar->latitude) && !empty($ar->longitude)) ? calculate_distance($latitude, $longitude, $ar->latitude, $ar->longitude)." km" : '';
												}
										}
										$ar->distance = $distance;
								}
								/* fill subdistrict_id*/
								if(isset($ar->subdistrict_id) && !empty($ar->subdistrict_id)){
										$sdists = $ci->region->findSubdistCond(['subcounty_code'=>$ar->subdistrict_id], 'subcounty_code as id, subcounty_name as name, IFNULL(zip_code, "") as zip_code');
										$ar->subdistrict = $sdists;
										unset($ar->subdistrict_id);
								}
								/* fill district_id*/
								if(isset($ar->district_id) && !empty($ar->district_id)){
										$dists = $ci->region->findDistCond(['county_code'=>$ar->district_id], 'county_code as id, county_name as name');
										$ar->district = $dists;
										unset($ar->district_id);
								}
								/* fill city_id*/
								if(isset($ar->city_id) && !empty($ar->city_id)){
										$cits = $ci->region->findCityCond(['city_code'=>$ar->city_id], 'city_code as id, city_name as name');
										$ar->city = $cits;
										unset($ar->city_id);
								}
								/* fill prov_id*/
								if(isset($ar->province_id) && !empty($ar->province_id)){
										$provs = $ci->region->findProvCond(['province_code'=>$ar->province_id], 'province_code as id, name');
										$ar->province = $provs;
										unset($ar->province_id);
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('singleGrosirOnly')){
		function singleGrosirOnly($ar){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->region)) $ci->load->model("M_Region", "region");
				$data = [];
				if(!empty($ar)){
								/* fill area*/
								if(isset($ar->area_service) && !empty($ar->area_service)){
										$areas = $ci->region->findAreaCond(['id'=>$ar->area_service], 'id, name');
										$ar->area_service = $areas;
								}
								/* avatar grosir*/
								if(isset($ar->avatar) && !empty($ar->avatar)){
										$aravat = (isset($ar->avatar) && !empty($ar->avatar) && strpos($ar->avatar, 'http') !== false) ? $ar->avatar : "https://".$ar->avatar;
								}else $aravat = base_url('contents/assets/img/grosir_def.jpg');
								$ar->avatar = $aravat;
								/* avatar grosir*/
								if(!isset($ar->photos)){
										$grosBan = $ci->grosir->getBanner(['grosir_id'=>$ar->id], 'banner_image as image');
										if(!empty($grosBan)){
												$daphot = [];
												foreach($grosBan as $kgab=>$gban){
														$defban = (strpos($gban["image"], 'http') !== false) ? $gban["image"] : "https://".$gban["image"];
														$daphot[] = ['image'=>$defban, 'default'=>($kgab == 0) ? true : false];
												}
												if(count($daphot) < 4){
													$totdap = 4 - count($daphot);
													$daphot2 = [];
													for($iphot=0;$iphot<$totdap;$iphot++){
															$daphot2[] = ['image'=> base_url('contents/assets/img/grosir_banner_def_1.jpg'), 'default'=>false];
													}
													$daphot = array_merge($daphot, $daphot2);
												}
												$ar->photos = $daphot;
										}else{
												$aravat = "";
												if(isset($ar->avatar) && !empty($ar->avatar)){
														$aravat = (isset($ar->avatar) && !empty($ar->avatar) && strpos($ar->avatar, 'http') !== false) ? $ar->avatar : "https://".$ar->avatar;
												}

												$grosav = (!empty($aravat)) ? $aravat : base_url('contents/assets/img/grosir_def.jpg');
												$ar->photos = [
														['image'=> $grosav, 'default'=>true],
														['image'=> base_url('contents/assets/img/grosir_banner_def_1.jpg'), 'default'=>false],
														['image'=> base_url('contents/assets/img/grosir_banner_def_2.jpg'), 'default'=>false],
														['image'=> base_url('contents/assets/img/grosir_banner_def_3.jpg'), 'default'=>false]
												];
										}
								}
								/* fill subdistrict_id*/
								if(isset($ar->subdistrict_id) && !empty($ar->subdistrict_id)){
										$sdists = $ci->region->findSubdistCond(['subcounty_code'=>$ar->subdistrict_id], 'subcounty_code as id, subcounty_name as name, IFNULL(zip_code, "") as zip_code');
										$ar->subdistrict = $sdists;
										unset($ar->subdistrict_id);
								}
								/* fill district_id*/
								if(isset($ar->district_id) && !empty($ar->district_id)){
										$dists = $ci->region->findDistCond(['county_code'=>$ar->district_id], 'county_code as id, county_name as name');
										$ar->district = $dists;
										unset($ar->district_id);
								}
								/* fill city_id*/
								if(isset($ar->city_id) && !empty($ar->city_id)){
										$cits = $ci->region->findCityCond(['city_code'=>$ar->city_id], 'city_code as id, city_name as name');
										$ar->city = $cits;
										unset($ar->city_id);
								}
								/* fill prov_id*/
								if(isset($ar->province_id) && !empty($ar->province_id)){
										$provs = $ci->region->findProvCond(['province_code'=>$ar->province_id], 'province_code as id, name');
										$ar->province = $provs;
										unset($ar->province_id);
								}

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('singleBrands')){
	function singleBrands($ar){
			$ci =& get_instance();
			if(!isset($ci->product)) $ci->load->model("M_Product", "product");
			$data = null;
			if(!empty($ar)){
					/* get data category 1 */
					if(isset($ar->principalId)){
							$princ = $ci->product->findPrincipalByCond(['id'=>$ar->principalId], 'id, code, name');
							$ar->principal = $princ;
							unset($ar->principalId);
					}
					$cats = [];
					/* get data category 1 */
					if(isset($ar->category1Id)){
							$cat1 = $ci->product->findCat1ByCond(['id'=>$ar->category1Id], 'id, code, name, 1 as level');
							$cats[] = $cat1;
							unset($ar->category1Id);
					}
					/* get data category 2 */
					if(isset($ar->category2Id)){
							$cat2 = $ci->product->findCat2ByCond(['id'=>$ar->category2Id], 'id, code, name, 2 as level');
							$cats[] = $cat2;
					}
					unset($ar->category2Id);
					/* get data category 3 */
					if(isset($ar->category3Id)){
							$cat3 = $ci->product->findCat3ByCond(['id'=>$ar->category3Id], 'id, code, name, 3 as level');
							$cats[] = $cat3;
					}
					unset($ar->category3Id);
					/* get data category 4 */
					if(isset($ar->category4Id)){
							$cat4 = $ci->product->findCat3ByCond(['id'=>$ar->category4Id], 'id, code, name, 4 as level');
							$cats[] = $cat4;
					}
					unset($ar->category4Id);
					$ar->category = $cats;
					$data = $ar;
			}

			return $data;
	}
}

if(!function_exists('singleProductOnly')){
		function singleProductOnly($ar){
				$ci =& get_instance();
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				$data = [];
				if(!empty($ar)){
								$deskripsi = '';
								$ar->qty = (!empty($ar->qty)) ? (double)$ar->qty : 1;
								/* fill image*/
								if(isset($ar->image)){
										$pcs_image = (!empty($ar->image)) ? $ar->image : 'https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg';
										if(strpos($pcs_image, 'http') !== false){
												$pcs_image = $pcs_image;
										}else $pcs_image = 'https://'.$pcs_image;
										$ar->image = $pcs_image;
								}
								/* fill brand*/
								if(isset($ar->brand_id)){
										$brands = $ci->product->findBrandByCond(['id'=>$ar->brand_id], 'id, code, name, principalId, category1Id, category2Id, category3Id, category4Id');
										$ar->brand = singleBrands($brands);
										unset($ar->brand_id);
								}
								/* fill pack*/
								if(isset($ar->packtype_id)){
										$packs = $ci->product->findPackTypeByCond(['id'=>$ar->packtype_id], 'id, code, name');
										$ar->packtype = $packs;
										unset($ar->packtype_id);
								}
								if(isset($ar->name)) $deskripsi .= 'SKU Name : '.$ar->name;
								if(isset($ar->brand->name)) $deskripsi .= '\nBrand : '.$ar->brand->name;
								if(isset($ar->brand->principal->name)) $deskripsi .= '\nPrincipal : '.$ar->brand->principal->name;
								if(isset($ar->brand->category) && count($ar->brand->category) > 0) {
										$catz = [];
										foreach($ar->brand->category as $ckz=>$ctz){
												if(isset($ctz->name)){
														$catz[] = $ctz->name;
												}
										}
										$catzz = implode(" / ", $catz);
										$deskripsi .= '\nCategory : '.$catzz;
								}
								if(isset($ar->qty) && !empty($ar->qty)) {
										$deskripsi .= '\nTotal Qty in Pouch : '.$ar->qty.' Pouch';
								}

								$ar->description = $deskripsi;

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourceTarget')){
		function resourceTarget($arr){
				$ci =& get_instance();
				if(!isset($ci->target)) $ci->load->model("M_Target", "target");
				$data = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								/* date month */
								if(isset($ar->target_month) && !empty($ar->target_month)){
											$ar->target_month = date("F Y", strtotime($ar->target_month."-1"));
								}

								$achieved = $ci->target->respCouponSingle('motorist_id = "'.$ar->motorist_id.'" AND (status = 2 OR status = 3)');

								if(!empty($achieved)){
										$ar->achieved = (isset($achieved->total)) ? (double)$achieved->total : 0;
								}else{
										$ar->achieved = 0;
								}

								/* checking target_amount*/
								if(isset($ar->target_amount)){
											$ar->target_amount = (double)$ar->target_amount;
											$percentage = ($ar->achieved / $ar->target_amount) * 100;
											$ar->percentage = ($percentage <= 100) ? round($percentage, 0) : 100;
											if($percentage >= 100) $ar->is_achieved = true;
											else $ar->is_achieved = false;

								}

								if(isset($ar->motorist_id)){
										unset($ar->motorist_id);
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourceTargetsingle')){
		function resourceTargetsingle($ar){
				$ci =& get_instance();
				if(!isset($ci->target)) $ci->load->model("M_Target", "target");
				$data = [];
				if(!empty($ar)){

								/* date month */
								if(isset($ar->target_month) && !empty($ar->target_month)){
											$tarmonth = $ar->target_month.'-1';
											$ar->target_month = date_id($ar->target_month."-1", 'F Y');
											$curmonth = date("Y-m-1");
											$ar->status = ($curmonth <= $tarmonth) ? 'ongoing' : 'expired';
											// $ar->tarmonth = $tarmonth;
											// $ar->curmonth = $curmonth;
								}

								$achieved = $ci->target->respCouponSingle('motorist_id = "'.$ar->motorist_id.'" AND status = 5');

								if(!empty($achieved)){
										$ar->achieved = (isset($achieved->total)) ? (double)$achieved->total : 0;
								}else{
										$ar->achieved = 0;
								}

								/* checking target_amount*/
								if(isset($ar->target_amount)){
											$ar->target_amount = (double)$ar->target_amount;
											$percentage = ($ar->achieved / $ar->target_amount) * 100;
											$ar->percentage = ($percentage <= 100) ? round($percentage, 0) : 100;
											if($percentage >= 100) $ar->is_achieved = true;
											else $ar->is_achieved = false;
											//check status
											if($ar->status == "ongoing" && $curmonth == $tarmonth && !empty($ar->target_amount)){
													$ar->status = "soon-expired";
											}

								}

								if(isset($ar->motorist_id)){
										unset($ar->motorist_id);
								}

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourceTargetDefault')){
		function resourceTargetDefault($date){
				$ci =& get_instance();
				$data = [];
				if(!empty($date)){
								$ar = (object)$data;
								/* date month */
								$ar->target_month = $date;
								if(!empty($ar->target_month)){
											$tarmonth = $ar->target_month.'-1';
											$ar->target_month = date_id($ar->target_month."-1", 'F Y');
											$curmonth = date("Y-m-1");
											$ar->status = ($curmonth <= $tarmonth) ? 'ongoing' : 'expired';
											// $ar->tarmonth = $tarmonth;
											// $ar->curmonth = $curmonth;
								}

								$ar->achieved = 0;

								/* checking target_amount*/
								$ar->target_amount = 0;
								$percentage = 0;
								$ar->percentage = ($percentage <= 100) ? round($percentage, 0) : 100;
								if($percentage >= 100) $ar->is_achieved = true;
								else $ar->is_achieved = false;
											//check status
								if($ar->status == "ongoing" && $curmonth == $tarmonth){
											$ar->status = "soon-expired";
								}

								$data = $ar;

				}
				return $data;
		}
}

if(!function_exists('resourceCart')){
		function resourceCart($arr, $motorist_id=''){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				if(!isset($ci->carts)) $ci->load->model("M_Cart", "carts");
				$data = [];
				$total = 0;
				if(!empty($arr)){
						foreach($arr as $ar){
								/* checking quantity*/
								if(isset($ar->quantity)){
											$ar->quantity = (double)$ar->quantity;
								}
								/* checking promo product*/
								if(isset($ar->id_promo_product)){
											if(isset($ar->grosir_id)){
													$grosirs = $ci->grosir->find($ar->grosir_id, 'id, name, IFNULL(latitude, "") as latitude, IFNULL(longitude, "") as longitude, IFNULL(address, "") as address');
													$ar->grosir = $grosirs;
													unset($ar->grosir_id);
											}
											$qty = (isset($ar->quantity)) ? $ar->quantity : 0;
											$prosql = $ci->promo->findProduct(["id"=>$ar->id_promo_product], "id, product_id, promo_id as id_promo, product_id, price_buy, price_before, (price_before - price_buy) as price_save, quantity, quantity_left, quantity_remain");
											if(isset($prosql->id)){
													$total += ($prosql->price_buy * $qty);
													$daprom = resourceProductCart($prosql);
													$ar->product = $daprom;
													unset($ar->id_promo_product);
											}
								}else if(isset($ar->grosir_id) && !isset($ar->id_promo_product) && !empty($motorist_id)){
										$grosirs = $ci->grosir->find($ar->grosir_id, 'id, name, district_code as district_id, city_code as city_id, IFNULL(address, "") as address, IFNULL(avatar, "") as avatar');
										if(isset($grosirs->id)){
												$grosirs = singleGrosirOnly($grosirs);
												$products = null;
												$citem = $ci->carts->list_items("grosir_id = '".$ar->grosir_id."' AND cart_id IN(SELECT id FROM carts WHERE motorist_id = '".$motorist_id."')", 'id, promo_product_id as id_promo_product, quantity');
												if(!empty($citem)){
														$prod_data = [];
														foreach($citem as $cite){
																$pprod = $ci->promo->findProduct("id = '".$cite->id_promo_product."'", 'id, promo_id, product_id, price_before, price_buy, (price_before - price_buy) as price_save, quantity, quantity_left, quantity_remain');
																if(isset($pprod->id)){
																		$subtotal = $pprod->price_buy * $cite->quantity;
																		$pprod->qty = (double)$cite->quantity;
																		$pprod->subtotal = (double)$subtotal;
																		$prod_data[] = resourceProductCart($pprod);
																		$total += $subtotal;
																}
														}
														$products = $prod_data;
												}
												$grosirs->products = $products;
												$ar->grosir = $grosirs;
												unset($ar->grosir_id);
										}else $ar->grosir = null;
								}
								$data[] = $ar;
						}
				}
				$ardata = ['carts'=>$data, 'total'=>$total];
				return $ardata;
		}
}

if(!function_exists('resourceCoupon')){
		function resourceCoupon($arr){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = [];
				$total = 0;
				if(!empty($arr)){
						foreach($arr as $ar){
								if(isset($ar->created_at)){
										$ar->created_at = date_id($ar->created_at, 'j M Y H:i');
								}
								/* checking price*/
								if(isset($ar->total_price)){
											$ar->total_price = (double)$ar->total_price;
								}
								/* checking save*/
								if(isset($ar->total_save)){
											$ar->total_save = (double)$ar->total_save;
								}
								/* checking quantity*/
								if(isset($ar->total_product)){
											$ar->total_product = (int)$ar->total_product;
								}
								/* checking quantity*/
								if(isset($ar->status)){
											switch($ar->status){
													case 0 : $status = "expired";break;
													case 1 : $status = "requested";break;
													case 2 : $status = "approved";break;
													case 3 : $status = "rejected";break;
													case 4 : $status = "redeemed";break;
													case 5 : $status = "success";break;
													default : $status = "requested";break;
											}
											$ar->status = $status;
								}

								$data_detail = $ci->db->select("IFNULL(SUM((price_motorist * coupon_products.quantity)), 0) as total_save, IFNULL(SUM(price_before), 0) as price_before")->join("promo_product", "coupon_products.promo_product_id = promo_product.id")->get_where("coupon_products", ["coupon_id"=>$ar->id]);
								if($data_detail && $data_detail->num_rows() > 0){
										$ar->total_save = (double)$data_detail->row()->total_save;
								}
								/* check grosir */
								if(isset($ar->id_grosir)){
										$grosirs = $ci->grosir->find($ar->id_grosir, 'id, name, IFNULL(latitude, "") as latitude, IFNULL(longitude, "") as longitude, IFNULL(address, "") as address, IFNULL(avatar, "") as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service');
										$ar->grosir = singleGrosirOnly($grosirs);
										unset($ar->id_grosir);
								}

								/* checking promo product*/
								if(isset($ar->id_promo)){
											$qty = (isset($ar->quantity)) ? $ar->quantity : 0;
											$prosql = $ci->promo->find(["id"=>$ar->id_promo], "id, code, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date");
											if(isset($prosql->id)){
													$ar->promo = $prosql;
													unset($ar->id_promo);
											}
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourceCouponProduct')){
		function resourceCouponProduct($arr){
				$ci =& get_instance();
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				$data = [];
				$total = 0;
				if(!empty($arr)){
						foreach($arr as $ar){
								/* checking price*/
								if(isset($ar->before_price)){
											$ar->before_price = (double)$ar->before_price;
								}
								/* checking price*/
								if(isset($ar->price)){
											$ar->price = (double)$ar->price;
								}
								/* checking subtotal*/
								if(isset($ar->subtotal)){
											$ar->subtotal = (double)$ar->subtotal;
								}
								/* checking total_save*/
								if(isset($ar->total_save)){
											$ar->total_save = (double)$ar->total_save;
								}
								/* checking comp_percent*/
								if(isset($ar->comp_percent)){
											$ar->comp_percent = (double)$ar->comp_percent;
								}

								if(isset($ar->promo_product_id)){
										$promdet = $ci->promo->findProduct(["id"=>$ar->promo_product_id], "id, product_id, promo_id, price_comp as comp_price, comp_motorist, comp_percent, price_motorist, price_buy, price_before, quantity, quantity_left, quantity_remain");
										if(isset($promdet->id)){
														$before_price = $promdet->price_before * $ar->quantity;
														$ar->before_price = $before_price;
														$total_save = $promdet->price_motorist * $ar->quantity;
														$ar->total_save = (double)$total_save;
														$ar->comp_percent = (double)$promdet->comp_motorist;
										}
								}
								/* checking quantity*/
								if(isset($ar->quantity)){
											$ar->quantity = (int)$ar->quantity;
											if(isset($ar->before_price)){
													$ar->total_before = $ar->before_price;
													$ar->before_price = $ar->before_price / $ar->quantity;
											}
											if(isset($ar->total_save)){
														$ar->save_price = $ar->total_save / $ar->quantity;
											}
								}
								/* check product */
								if(isset($ar->product_id)){
										$product = $ci->product->findCond(['id'=>$ar->product_id], 'id, name, IFNULL(pcs_image, "https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg") as image');
										$ar->product = $product;
										if(isset($ar->product->image)){
												if(strpos($ar->product->image, 'http') !== false){
														$ar->product->image = $ar->product->image;
												}else $ar->product->image = 'https://'.$ar->product->image;
										}
										unset($ar->product_id);
								}

								$data[] = $ar;
						}
				}
				return $data;
		}
}

if(!function_exists('resourceWarung')){
		function resourceWarung($arr){
				$ci =& get_instance();
				if(!isset($ci->region)) $ci->load->model("M_Region", "region");
				$data = [];
				if(!empty($arr)){
							foreach($arr as $ar){
									if(isset($ar->avatar) && !empty($ar->avatar)){
											$ar->avatar = (strpos($ar->avatar, 'placekitten.com') !== false) ? base_url()."contents/assets/img/warung_default.jpg" : $ar->avatar;
									}else{
											$ar->avatar = base_url()."contents/assets/img/warung_default.jpg";
									}
									/* fill subdistrict_id*/
									if(isset($ar->subdistrict_code) && !empty($ar->subdistrict_code)){
											$sdists = $ci->region->findSubdistCond(['subcounty_code'=>$ar->subdistrict_code], 'subcounty_code as id, subcounty_name as name, IFNULL(zip_code, "") as zip_code');
											$ar->subdistrict = $sdists;
											unset($ar->subdistrict_code);
									}
									/* fill district_id*/
									if(isset($ar->district_code) && !empty($ar->district_code)){
											$dists = $ci->region->findDistCond(['county_code'=>$ar->district_code], 'county_code as id, county_name as name');
											$ar->district = $dists;
											unset($ar->district_code);
									}
									/* fill city_id*/
									if(isset($ar->city_code) && !empty($ar->city_code)){
											$cits = $ci->region->findCityCond(['city_code'=>$ar->city_code], 'city_code as id, city_name as name');
											$ar->city = $cits;
											unset($ar->city_code);
									}
									/* fill prov_id*/
									if(isset($ar->province_code) && !empty($ar->province_code)){
											$provs = $ci->region->findProvCond(['province_code'=>$ar->province_code], 'province_code as id, name');
											$ar->province = $provs;
											unset($ar->province_code);
									}
									$data[] = $ar;
							}
				}
				return $data;
		}
}

if(!function_exists('resourceWarungSingle')){
		function resourceWarungSingle($ar){
				$ci =& get_instance();
				if(!isset($ci->region)) $ci->load->model("M_Region", "region");
				$data = [];
				if(!empty($ar)){
								/* fill subdistrict_id*/
								if(isset($ar->subdistrict_code) && !empty($ar->subdistrict_code)){
										$sdists = $ci->region->findSubdistCond(['subcounty_code'=>$ar->subdistrict_code], 'subcounty_code as id, subcounty_name as name, IFNULL(zip_code, "") as zip_code');
										$ar->subdistrict = $sdists;
										unset($ar->subdistrict_code);
								}
								/* fill district_id*/
								if(isset($ar->district_code) && !empty($ar->district_code)){
										$dists = $ci->region->findDistCond(['county_code'=>$ar->district_code], 'county_code as id, county_name as name');
										$ar->district = $dists;
										unset($ar->district_code);
								}
								/* fill city_id*/
								if(isset($ar->city_code) && !empty($ar->city_code)){
										$cits = $ci->region->findCityCond(['city_code'=>$ar->city_code], 'city_code as id, city_name as name');
										$ar->city = $cits;
										unset($ar->city_id);
								}
								/* fill prov_id*/
								if(isset($ar->province_code) && !empty($ar->province_code)){
										$provs = $ci->region->findProvCond(['province_code'=>$ar->province_code], 'province_code as id, name');
										$ar->province = $provs;
										unset($ar->province_code);
								}

								$data = $ar;
				}
				return $data;
		}
}


if(!function_exists('resourceInsightProduct')){
		function resourceInsightProduct($arr){
				$ci =& get_instance();
				if(!isset($ci->grosir)) $ci->load->model("M_Grosir", "grosir");
				if(!isset($ci->product)) $ci->load->model("M_Product", "product");
				if(!isset($ci->promo)) $ci->load->model("M_Promo", "promo");
				$data = $categorize = [];
				if(!empty($arr)){
						foreach($arr as $ar){
								$deskripsi = '';
								$ar->qty = (!empty($ar->qty)) ? (double)$ar->qty : 1;
								/* fill image*/
								if(isset($ar->image)){
										$pcs_image = (!empty($ar->image)) ? $ar->image : 'https://dev-motorist.pintap.id/contents/assets/img/produk_def.jpg';
										if(strpos($pcs_image, 'http') !== false){
												$pcs_image = $pcs_image;
										}else $pcs_image = 'https://'.$pcs_image;
										$ar->image = $pcs_image;
								}
								/* fill brand*/
								if(isset($ar->brand_id)){
										$brands = $ci->product->findBrandByCond(['id'=>$ar->brand_id], 'id, code, name, principalId, category1Id, category2Id, category3Id, category4Id');
										$ar->brand = singleBrands($brands);
										unset($ar->brand_id);
								}
								/* fill pack*/
								if(isset($ar->packtype_id)){
										$packs = $ci->product->findPackTypeByCond(['id'=>$ar->packtype_id], 'id, code, name');
										$ar->packtype = $packs;
										unset($ar->packtype_id);
								}
								if(isset($ar->name)) $deskripsi .= 'SKU Name : '.$ar->name;
								if(isset($ar->brand->name)) $deskripsi .= '\nBrand : '.$ar->brand->name;
								if(isset($ar->brand->principal->name)) $deskripsi .= '\nPrincipal : '.$ar->brand->principal->name;
								if(isset($ar->brand->category) && count($ar->brand->category) > 0) {
										$catz = [];
										foreach($ar->brand->category as $ckz=>$ctz){
												if(isset($ctz->name)){
														$catz[] = $ctz->name;
												}
										}
										$scatz = end($catz);
										$total_bought = (isset($ar->total_buy)) ? $ar->total_buy : 1;
										if(isset($categorize[$scatz])) $categorize[$scatz] = $categorize[$scatz] + $total_bought;
										else $categorize[$scatz] = (int)$total_bought;
										$catzz = implode(" / ", $catz);
										$deskripsi .= '\nCategory : '.$catzz;
								}

								if(isset($ar->total_buy)) $ar->total_buy = (int)$ar->total_buy;
								if(isset($ar->total_price)) $ar->total_price = (double)$ar->total_price;

								if(isset($ar->qty) && !empty($ar->qty)) {
										$deskripsi .= '\nTotal Qty in Pouch : '.$ar->qty.' Pouch';
								}

								$ar->description = $deskripsi;

								$data[] = $ar;
						}
				}
				arsort($categorize);
				return ['product'=>array_slice($data, 0, 5), 'category'=>array_slice($categorize, 0, 5)];
		}
}

if(!function_exists('resourcePoint')){
		function resourcePoint($arr){
				$ci =& get_instance();
				if(!isset($ci->chall)) $ci->load->model("M_Challenge", "chall");
				if(!isset($ci->coupon)) $ci->load->model("M_Voucher", "coupon");
				$data = [];
				if(!empty($arr)){
							foreach($arr as $ar){
									/* checking created at */
									if(isset($ar->created_at)){
												$ar->created_at = date_id($ar->created_at, 'j M Y H:i');
									}
									/* checking expired at */
									if(isset($ar->expired_at)){
												$ar->expired_at = date_id($ar->expired_at, 'j M Y H:i');
									}
									/* checking amount */
									if(isset($ar->amount)){
												$ar->amount = (double)$ar->amount;
									}
									/* fill source */
									$sdata = null;
									$ar->source = (string)$ar->source;
									$ar->source_id = (string)$ar->source_id;
									if(!empty($ar->source) && !empty($ar->source_id)){
											switch($ar->source){
													case 'challenge' : $sdata = $ci->chall->findCond(['id'=>$ar->source_id], 'id, subject as name');break;
													case 'coupon' : $sdata = $ci->coupon->findCond(['id'=>$ar->source_id], 'id, coupon_no as name');break;
													default : $sdata = $ci->chall->findCond(['id'=>$ar->source_id], 'id, subject as name');break;
											}
									}
									$ar->source_data = $sdata;

									$data[] = $ar;
							}
				}
				return $data;
		}
}
