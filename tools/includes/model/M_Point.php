<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Point extends CI_Model{

			protected $table;

			function __construct(){
					parent::__construct();
					$this->table = 'motorist_points';
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function delete($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function history($id, $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					$pdata = $this->db->get_where($this->table, array("motorist_id"=>$id));
					$data_point = [];
					if($pdata && $pdata->num_rows() > 0){
							$data_point = $pdata->result();
					}
					return $data_point;
			}

			function find($id) {
					$usdata = $this->db->get_where($this->table, array("id"=>$id));
					return $usdata->row();
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}
}
