<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImportModel extends CI_Model {

  protected $table_user;
  protected $table_contract;

  function __construct(){
      parent::__construct();
      $this->table_user = 'users';
      $this->table_contract = 'plan_subscriptions';
  }

	public function insert_user($data){
		$insert = $this->db->insert_batch($this->table_user, $data);
		if($insert){
			return true;
		}
	}
	public function insert_contract($data){
		$insert = $this->db->insert_batch($this->table_contract, $data);
		if($insert){
			return true;
		}
	}
	public function getDataUser(){
		$this->db->select('*');
		return $this->db->get($this->table_user)->result_array();
	}

}
