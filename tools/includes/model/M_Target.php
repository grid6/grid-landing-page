<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Target extends CI_Model{

			protected $table;
			protected $table_voucher;
			protected $table_voucher_products;

			function __construct(){
					parent::__construct();
					$this->table = 'motorist_target';
					$this->table_voucher = 'coupons';
					$this->table_voucher_products = 'coupon_products';
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function delete($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table, $cond);
			}

			function find($id) {
					$usdata = $this->db->get_where($this->table, array("id"=>$id));
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function lists($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					$sodata = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					return $sodata;
			}

			function respCouponSingle($where, $select='', $join='', $onjoin='') {
					if(!empty($select)) $this->db->select($select);
					else $this->db->select("SUM(total_price) as total");
					if(!empty($join) && !empty($onjoin)) $this->db->join($join, $onjoin);
					$usdata = $this->db->get_where($this->table_voucher, $where);
					$sodata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $sodata;
			}
}
