<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Promo extends CI_Model{

			protected $table;
			protected $table_detail;
			protected $table_prod;

			function __construct(){
					parent::__construct();
					$this->table = 'promos';
					$this->table_detail = 'promo_product';
					$this->table_prod = 'products';
					$this->load->model("M_Grosir", "grosir");
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function update_product($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table_detail, $data, $cond);
			}

			function delete($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table, $cond);
			}

			function find($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					if(!is_array($where)){
							$where = array('id'=>$where);
					}
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function findProduct($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					if(!is_array($where) && strpos($where, ' ') === false){
							$where = array('id'=>$where);
					}
					$usdata = $this->db->get_where($this->table_detail, $where);
					return $usdata->row();
			}

			function findProductCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_detail, $where);
					return $usdata->row();
			}

			function lists($cond=null, $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					$this->db->limit($rpp, $spage);
					$this->db->order_by("code", "ASC");
					// $this->db->order_by("end_date", "ASC");
					if(!empty($cond)) $this->db->where($cond);
					$rsdata = $this->db->select("id, code, grosir_id, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date")->get($this->table);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];
					return $data_ret;
			}

			function listordgros($cond="", $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					$limit = "LIMIT $spage, $rpp";
					$whereadj = "";
					if(!empty($cond)){
							$whereadj = " AND $cond";
					}
					$query = "select *
					from (
					    select '1' as `rank`, id, code, grosir_id, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date, created_by_role, created_at from promos where `status` = 'ACTIVE' and created_by_role = 'GROSIR'$whereadj
					    union all
					    select '1' as `rank`, id, code, grosir_id, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date, created_by_role, created_at from promos where `status` = 'ACTIVE' and created_by_role != 'GROSIR'$whereadj
					) a
					order by `rank` asc, end_date asc limit $spage, $rpp
					";
					$rsdata = $this->db->query($query);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];
					return $data_ret;
			}

			function totaldata($cond=null){
					if(!empty($cond)) $this->db->where($cond);
					$rsdata = $this->db->select("COUNT(id) as total")->get($this->table);
					$tot = ($rsdata && $rsdata->num_rows() > 0) ? (int)$rsdata->row()->total : 0;
					return $tot;
			}

			function list_product($cond=null, $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					// $this->db->order_by("id", "DESC");
					if(!empty($cond)) $this->db->where($cond);
					$this->db->order_by($this->table.".end_date", "DESC");
					$this->db->join($this->table, $this->table.".id = ".$this->table_detail.".promo_id");
					$rsdata = $this->db->select($this->table_detail.".id as id, promo_id, product_id, price_before, price_buy, quantity_left")->get($this->table_detail);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];

					return $data_ret;
			}

			function totalproduct($cond=null){

					if(!empty($cond)) $this->db->where($cond);
					$this->db->order_by($this->table.".end_date", "DESC");
					$this->db->join($this->table, $this->table.".id = ".$this->table_detail.".promo_id");
					$this->db->select("COUNT(promo_product.id) as total");
					$rsdata = $this->db->get($this->table_detail);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->row()->total : 0;

					return $data_ret;
			}

			function cari_product($cond=null, $rpp=5, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					$this->db->order_by($this->table.".end_date", "DESC");
					$this->db->join($this->table, $this->table.".id = ".$this->table_detail.".promo_id");
					$rsdata = $this->db->select($this->table_detail.".id as id, product_id, price_before, price_buy, quantity_left")->get($this->table_detail);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];

					return $data_ret;
			}

			function list_productgros($cond=null, $rpp=10, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					$this->db->order_by($this->table_detail.".quantity_left", "DESC");
					$this->db->order_by($this->table.".end_date", "ASC");
					$this->db->join($this->table, $this->table.".id = ".$this->table_detail.".promo_id");
					$rsdata = $this->db->select($this->table_detail.".id as id, promo_id as id_promo, product_id, price_before, price_buy, quantity_left, quantity_remain")->get($this->table_detail);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];

					return $data_ret;
			}

			function total_productgros($cond=null){
					$this->db->join($this->table, $this->table.".id = ".$this->table_detail.".promo_id");
					if(!empty($cond)) $this->db->where($cond);
					$rsdata = $this->db->select("COUNT(".$this->table_detail.".id) as total")->get($this->table_detail);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->row()->total : 0;
					return $data_ret;
			}

			function detail($id){
					$data_show = null;
					$cdata = $this->db->select("id, grosir_id, code, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date")->get_where($this->table, array("id"=>$id));
					if($cdata && $cdata->num_rows() > 0){
							$data_show = $cdata->row();
							$wherez = ["promo_id"=>$id];
							$this->db->order_by("quantity_left", "DESC");
							$ddata = $this->db->select("id, price_before, price_buy, product_id, quantity_left")->get_where($this->table_detail, $wherez);
							$data = ($ddata && $ddata->num_rows() > 0) ? $ddata->result() : [];
							$data_show->product = resourceProduct($data);
					}
					return $data_show;
			}
}
