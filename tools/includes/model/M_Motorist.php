<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Motorist extends CI_Model{

			protected $table;
			protected $table_warung;
			protected $table_sales;

			function __construct(){
					parent::__construct();
					$this->table = 'users_motorist';
					$this->table_warung = 'warungs';
					$this->table_sales = 'warung_sales';
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function insight($id){
					$pdata = $this->db->select("IFNULL(SUM(amount), 0) as total")->get_where("motorist_points", array("motorist_id"=>$id));
					$total_point = ($pdata && $pdata->num_rows() > 0) ? $pdata->row()->total : 0;
					$wdata = $this->db->select("COUNT(id) as total")->get_where("warungs", array("input_by"=>$id));
					$total_warung = ($wdata && $wdata->num_rows() > 0) ? $wdata->row()->total : 0;
					$trdata = $this->db->select("IFNULL(SUM(total_price), 0) as total")->get_where("coupons", "motorist_id = '$id' AND status > 3 AND (MONTH(created_at) = MONTH(NOW()) AND YEAR(created_at) = YEAR(NOW()))");
					$total_trans = ($trdata && $trdata->num_rows() > 0) ? $trdata->row()->total : 0;
					$svdata = $this->db->select("IFNULL(SUM(total_save), 0) as total")->get_where("coupons", "motorist_id = '$id' AND status > 3");
					$total_save = ($svdata && $svdata->num_rows() > 0) ? $svdata->row()->total : 0;
					$target_month = date("Y-m");
					$tgdata = $this->db->select("target_amount as total")->get_where("motorist_target", "motorist_id = '$id' AND target_month = '$target_month'");
					$total_target = ($tgdata && $tgdata->num_rows() > 0) ? $tgdata->row()->total : 0;
					$data_insight = [
						'total_point' => $total_point,
						'total_target' => "IDR. ".number_format($total_target, 0 , ",", "."),
						'total_save' => "IDR. ".number_format($total_save, 0 , ",", "."),
						'trans_monthly' => "IDR. ".number_format($total_trans, 0 , ",", ".")
					];
					return $data_insight;
			}

			function point_history($id){
					$pdata = $this->db->get_where("motorist_points", array("motorist_id"=>$id));
					$data_point = [];
					if($pdata && $pdata->num_rows() > 0){
							$data_point = $pdata->result();
					}
					return $data_point;
			}

			function find($id) {
					$usdata = $this->db->select("id, phone, IFNULL(first_name, '') as first_name, IFNULL(last_name, '') as last_name, IFNULL(identity, '') as nik, IFNULL(birthdate, '') as birthdate, IFNULL(area_operational, '') as area_operational, IFNULL(avatar, '') as avatar, IFNULL(address, '') as address, IFNULL(gender, '') as gender, created_at, IFNULL(updated_at , '') as updated_at")->get_where($this->table, array("id"=>$id));
					return $usdata->row();
			}

			function location($id) {
					$usdata = $this->db->select("IFNULL(area_operational, '') as area_operational, IFNULL(province_code, '') as province_code, IFNULL(city_code, '') as city_code, IFNULL(district_code , '') as district_code, IFNULL(subdistrict_code , '') as subdistrict_code, IFNULL(zip_code , '') as zip_code, IFNULL(address, '') as address")->get_where($this->table, array("id"=>$id));
					return $usdata->row();
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function getWarung($id, $name='') {
					if(!empty($name)) $this->db->like("name", $name);
					$usdata = $this->db->select("id, phone, name, avatar, owner, is_retail, IFNULL(address, '') as address, city_code, latitude, longitude, notes")->get_where($this->table_warung, array("input_by"=>$id));
					$warung = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					if(!empty($warung)){
							foreach($warung as $wr){
									$wr->is_retail = (isset($wr->is_retail) && !empty($wr->is_retail)) ? true : false;
							}
					}
					return $warung;
			}

			function getWarungBestSeller($id, $name='') {
					if(!empty($name)) $this->db->like("name", $name);
					$usdata = $this->db->select("id, phone, name, avatar, owner, is_retail, city_code, latitude, longitude, notes")->get_where($this->table_warung, "input_by = '$id' AND id IN(SELECT warung_id FROM ".$this->table_sales." WHERE price > 0)");
					$warung = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					if(!empty($warung)){
							foreach($warung as $wr){
									$wr->is_retail = (isset($wr->is_retail) && !empty($wr->is_retail)) ? true : false;
									$wrsales = $this->db->select("COUNT(id) as count_sales, IFNULL(SUM(price), 0) as total_sales")->get_where($this->table_sales, ["warung_id"=>$wr->id]);
									$wr->total_sales = ($wrsales && $wrsales->num_rows() > 0) ? (double)$wrsales->row()->total_sales : 0;
									$wr->count_sales = ($wrsales && $wrsales->num_rows() > 0) ? (double)$wrsales->row()->count_sales : 0;
							}
					}
					return $warung;
			}
}
