<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Warung extends CI_Model{

			protected $table;
			protected $table_sales;

			function __construct(){
					parent::__construct();
					$this->table = 'warungs';
					$this->table_sales = 'warung_sales';
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function delete($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table, $cond);
			}

			function lists($cond=null, $select='', $rpp=20, $page=1) {
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get($this->table);
					$warung = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					if(!empty($warung)){
							foreach($warung as $wr){
									$wr->is_retail = (isset($wr->is_retail) && !empty($wr->is_retail)) ? true : false;
							}
					}
					return $warung;
			}

			function find($id) {
					$usdata = $this->db->select("id, name, phone, owner, IFNULL(avatar, '') as avatar, city_code, IFNULL(areas, '') as area, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(notes, '') as notes, input_by, is_retail, created_at, updated_at")->get_where($this->table, array("id"=>$id));
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					// if(isset($rowdata->input_by)){
					// 		$mot = $this->db->select("id, IFNULL(name, '') as first_name, IFNULL(last_name, '') as last_name, IFNULL(avatar, '') as avatar")->get_where($this->table_motor, ["id"=>$rowdata->input_by]);
					// 		$rowdata->input_by = ($mot && $mot->num_rows() > 0) ? $mot->row() : $rowdata->input_by;
					// }
					return $rowdata;
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function getSales($id, $sid) {
					$this->db->order_by("date", "desc");
					$usdata = $this->db->get_where($this->table_sales, array("warung_id"=>$id, "input_by"=>$sid));
					$sales = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					return $sales;
			}

			function listByArea($uid=0, $rpp=20, $page=1, $name=''){
					$data_ret = [];
					if(!empty($uid)){
							$andWhereNames = (!empty($name)) ? " AND name LIKE '%$name%'" : "";
							$motor = $this->motor->findCond(["id"=>$uid]);
							if(isset($motor->id)){
									$latlong = $motor->last_location;

									if(!empty($latlong)){
											$lalo = explode(",", $latlong);
											$latitude = $lalo[0];
											$longitude = (isset($lalo[1])) ? $lalo[1] : '';
											if(!empty($longitude)){
													$daret = $disid = [];
													$data_loc = get_location($latitude, $longitude);
													if(isset($data_loc["district"])){
															$disdis = $data_loc["district"];
															$dists = $this->db->select("county_code as id")->get_where("districts", "county_name LIKE '%$disdis%'");
															if($dists && $dists->num_rows() > 0){
																	$gros1 = $this->lists("input_by = '$uid' AND district_code = '".$dists->row()->id."'$andWhereNames", "id, name, IFNULL(avatar, '') as avatar, city_code, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude");
																	if(!empty($gros1)){
																			foreach($gros1 as $gr1){
																					$disid[] = "'".$gr1->id."'";
																					$distanz = (!empty($latitude) && !empty($longitude) && !empty($gr1->latitude) && !empty($gr1->longitude)) ? calculate_distance($latitude, $longitude, $gr1->latitude, $gr1->longitude) : '';
																					$gr1->distance = $distanz;
																					$daret[] = $gr1;
																			}
																	}
															}
													}

													if(isset($data_loc["city"]) && count($daret) < 5){
															$citcit = $data_loc["city"];
															$cits = $this->db->select("city_code as id")->get_where("cities", "city_name LIKE '%$citcit%'");
															if($cits && $cits->num_rows() > 0){
																	foreach($cits->result() as $cit){
																			$city_id = $cit->id;
																			$whereadj = "";
																			if(!empty($disid)){
																					$listid = implode(",", $disid);
																					$whereadj = " AND id NOT IN($listid)";
																			}
																			$gros2 = $this->lists("input_by = '$uid' AND city_code = '$city_id'$whereadj".$andWhereNames, "id, name, IFNULL(avatar, '') as avatar, city_code, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude");
																			foreach($gros2 as $gr2){
																					$distanz = (!empty($latitude) && !empty($longitude) && !empty($gr2->latitude) && !empty($gr2->longitude)) ? calculate_distance($latitude, $longitude, $gr2->latitude, $gr2->longitude) : '';
																					$gr2->distance = $distanz;
																					$daret[] = $gr2;
																			}
																	}
															}
													}
													array_multisort( array_column($daret, "distance"), SORT_ASC, $daret );
													$data_ret = $daret;
											}
									}else{
											$daret = $disid = [];
											if(!empty($motor->area_operational)){
													$area = $motor->area_operational;
													$area_find = $this->db->get_where("area_services", ["id"=>$area]);
													if($area_find && $area_find->num_rows() > 0){
															$city_id = $area_find->row()->city_code;
															$gros = $this->lists("input_by = '$uid' AND city_code = '$city_id'$andWhereNames", "id, name, IFNULL(avatar, '') as avatar, city_code, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude");
															foreach($gros as $gr){
																	$gr->distance = '';
																	$daret[] = $gr;
															}
													}
											}
											// if(!empty($motor->city_code)){
											// 		$city_id = $motor->city_code;
											// 		$whereadj = "";
											// 		if(!empty($disid)){
											// 				$listid = implode(",", $disid);
											// 				$whereadj = " AND id NOT IN($listid)";
											// 		}
											// 		$gros2 = $this->lists("city_code = '$city_id'$whereadj", "id, name, IFNULL(avatar, '') as avatar, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude");
											// 		foreach($gros2 as $gr2){
											// 				$gr2->distance = '';
											// 				$daret[] = $gr2;
											// 		}
											// }
											$data_ret = $daret;
									}
							}
					}
					return $data_ret;
			}
}
