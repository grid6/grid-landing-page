<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Product extends CI_Model{

			protected $table;
			protected $table_brand;
			protected $table_packtype;
			protected $table_principal;
			protected $table_cat1;
			protected $table_cat2;
			protected $table_cat3;
			protected $table_cat4;

			function __construct(){
					parent::__construct();
					$this->table = 'products';
					$this->table_brand = 'brands';
					$this->table_packtype = 'packtypes';
					$this->table_principal = 'principals';
					$this->table_cat1 = 'categories_lv1';
					$this->table_cat2 = 'categories_lv2';
					$this->table_cat3 = 'categories_lv3';
					$this->table_cat4 = 'categories_lv4';
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function delete($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table, $cond);
			}

			function find($id, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, array("id"=>$id));
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findPrincipalByCond($cond, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_principal, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findBrandByCond($cond, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_brand, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCat1ByCond($cond, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_cat1, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCat2ByCond($cond, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_cat2, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCat3ByCond($cond, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_cat3, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCat4ByCond($cond, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_cat4, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findPackTypeByCond($cond, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_packtype, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function lists($where, $select='', $rpp=20, $page=1) {
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					$sodata = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					return $sodata;
			}

			function total($where) {
					$this->db->select("COUNT(id) as total");
					$usdata = $this->db->get_where($this->table, $where);
					$sodata = ($usdata && $usdata->num_rows() > 0) ? (int)$usdata->row()->total : 0;
					return $sodata;
			}
}
